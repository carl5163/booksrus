package comp4410.booksRUs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

@SuppressWarnings("serial")
public class AddCustomerDialog extends JDialog
{
	private JComponent parent;
	private Connection con;
	private JButton btnSave;
	private JTextField tfName;
	private JTextField tfEmail;
	private JTextField tfPhone;
	private JTextField tfStreet;
	private JTextField tfCity;
	private JTextField tfState;
	private JTextField tfZip;
	
	public AddCustomerDialog(JComponent parent, Connection con)
	{
		this.parent = parent;
		this.con = con;
		setupDialog();
		setTitle("Add Customer");
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setMinimumSize(new Dimension(520, 224));
		setPreferredSize(new Dimension(520, 224));
		setVisible(true);
	}

	private void setupDialog()
	{
		getContentPane().setLayout(new BorderLayout());
		JPanel dialogPane = new JPanel();
		
		DocumentListener doc = new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						JTextField owner = (JTextField) e.getDocument().getProperty("owner");
						int len = (int) e.getDocument().getProperty("len");
						if(owner.getText().trim().length() == 0) {
							btnSave.setEnabled(false);
						} else {
							if(tfName.getText().trim().length() > 0 && tfEmail.getText().trim().length() > 0 && tfPhone.getText().trim().length() > 0 && tfStreet.getText().trim().length() > 0 && tfCity.getText().trim().length() > 0 && tfState.getText().trim().length() > 0 && tfZip.getText().trim().length() > 0) {
								btnSave.setEnabled(true);
							}
							if(owner.getText().trim().length() > len) {
								owner.setText(owner.getText().trim().substring(0, len));
							}
						}
					}
					
				});
				
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				if(((JTextField)(e.getDocument().getProperty("owner"))).getText().trim().length() == 0) {
					btnSave.setEnabled(false);
				}
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		};
		
		
		JLabel lblName = new JLabel("Name:");
		tfName = new JTextField();
		tfName.setPreferredSize(new Dimension(180,25));
		tfName.getDocument().putProperty("owner", tfName);
		tfName.getDocument().putProperty("len", 30);
		tfName.getDocument().addDocumentListener(doc);
		
		JLabel lblEmail = new JLabel("Email:");
		tfEmail = new JTextField();
		tfEmail.getDocument().putProperty("owner", tfEmail);
		tfEmail.getDocument().putProperty("len", 255);
		tfEmail.getDocument().addDocumentListener(doc);
		tfEmail.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				tfEmail.setBackground(Color.WHITE);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				tfEmail.setBackground(Color.WHITE);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				tfEmail.setBackground(Color.WHITE);
			}
			
		});
		
		JLabel lblPhone = new JLabel("Phone:");
		tfPhone = new JTextField();
		tfPhone.getDocument().putProperty("owner", tfPhone);
		tfPhone.getDocument().putProperty("len", 20);
		tfPhone.getDocument().addDocumentListener(doc);
		
		JLabel lblStreet = new JLabel("Street:");
		tfStreet = new JTextField();
		tfStreet.getDocument().putProperty("owner", tfStreet);
		tfStreet.getDocument().putProperty("len", 255);
		tfStreet.getDocument().addDocumentListener(doc);
		
		JLabel lblCity = new JLabel("City:");
		tfCity = new JTextField();
		tfCity.getDocument().putProperty("owner", tfCity);
		tfCity.getDocument().putProperty("len", 50);
		tfCity.getDocument().addDocumentListener(doc);
		
		JLabel lblState = new JLabel("State:");
		tfState = new JTextField();
		tfState.getDocument().putProperty("owner", tfState);
		tfState.getDocument().putProperty("len", 2);
		tfState.getDocument().addDocumentListener(doc);
		
		JLabel lblZip = new JLabel("Zip Code:");
		tfZip = new JTextField();
		tfZip.getDocument().putProperty("owner", tfZip);
		tfZip.getDocument().putProperty("len", 10);
		tfZip.getDocument().addDocumentListener(doc);

		btnSave = new JButton("Save");
		btnSave.setPreferredSize(new Dimension(180,25));
		btnSave.setEnabled(false);
		btnSave.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try {
					String query;
					Statement st = con.createStatement();
					String email = tfEmail.getText().trim();
					String password = "changeme";
					String name = tfName.getText().trim();
					String phone = tfPhone.getText().trim();
					String street = tfStreet.getText().trim();
					String city = tfCity.getText().trim();
					String state = tfState.getText().trim();
					String zip = tfZip.getText().trim();
					
					query = String.format("Insert Into customers Values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", email, name, password, phone, street, city, state, zip);
					st.executeUpdate(query);
					dispose();
				} catch (SQLException e1) {
					if(e1.getMessage().toUpperCase().contains("DUPLICATE")) {
						tfEmail.setBackground(Color.RED);
						JOptionPane.showMessageDialog(null, "That email is already in use.", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setPreferredSize(new Dimension(180,25));
		btnCancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();
			}
		});
		
		GroupLayout layout = new GroupLayout(dialogPane);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		layout.setHorizontalGroup(
			layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup()
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblName)
								.addComponent(tfName)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblEmail)
								.addComponent(tfEmail)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblPhone)
								.addComponent(tfPhone)
							)
						)
						.addGap(20, 20, 1000)
						.addGroup(layout.createParallelGroup()
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblStreet)
								.addComponent(tfStreet)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblCity)
								.addComponent(tfCity)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblState)
								.addComponent(tfState)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblZip)
								.addComponent(tfZip)
							)
						)
					)
					.addGroup(layout.createSequentialGroup()
						.addGap(40)
						.addComponent(btnCancel)
						.addGap(10, 10, 1000)
						.addComponent(btnSave)
						.addGap(50)
					)
				)
		);

		layout.setVerticalGroup(
			layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblName)
								.addComponent(tfName)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblEmail)
								.addComponent(tfEmail)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblPhone)
								.addComponent(tfPhone)
							)
						)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblStreet)
								.addComponent(tfStreet)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblCity)
								.addComponent(tfCity)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblState)
								.addComponent(tfState)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblZip)
								.addComponent(tfZip)
							)
						)
					)
					.addGap(20, 20, 1000)
					.addGroup(layout.createParallelGroup()
						.addComponent(btnCancel)
						.addComponent(btnSave)
					)
				)
		);
		
		layout.linkSize(SwingConstants.HORIZONTAL, lblName, lblPhone, lblEmail, lblCity, lblStreet, lblState, lblZip);
		layout.linkSize(SwingConstants.HORIZONTAL, btnCancel, btnSave, tfName, tfEmail, tfPhone, tfStreet, tfCity, tfState, tfZip);
		layout.linkSize(SwingConstants.VERTICAL, btnCancel, btnSave, lblName, lblPhone, lblEmail, lblCity, lblStreet, lblState, lblZip, tfName, tfEmail, tfPhone, tfStreet, tfCity, tfState, tfZip);
		
		dialogPane.setLayout(layout);
		
		this.add(dialogPane);
		setSize(new Dimension(400,400));
		setLocationRelativeTo(parent);
		pack();
	}
}
