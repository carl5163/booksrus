package comp4410.booksRUs;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@SuppressWarnings("serial")
public class OrderedBookPanel extends BookPanel
{
	double date;
	
	public OrderedBookPanel(CartPanel p, Book b, String date, String pubAddress, String pubPhone, String authAddress, boolean admin, Connection con, String orderNum) throws SQLException 
	{
		super(p, b, pubAddress, pubPhone, authAddress, admin, con, true);
		
		lblQuantityInStockLabel.setText("Quantity Ordered:");
		lblPrice.setText(String.format("$%.2f", b.price));
		lblYearLabel.setText("Date Ordered:");
		lblYear.setText(date);

		String street, city, state, zip;		
		String query = String.format("Select DISTINCT * From OrderTransactions O NATURAL JOIN Customers WHERE O.ordernum = %s", orderNum);
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);		
		rs.next();
		
		street = rs.getString(rs.findColumn("address"));
		city = rs.getString(rs.findColumn("city"));
		state = rs.getString(rs.findColumn("state"));
		zip = rs.getString(rs.findColumn("zip"));		
		
		lblPublisherLabel.setText("Order Number:");
		lblPublisher.setText(orderNum); // Actual order number here
		
		lblAuthorLabel.setText("Shipped To:");
		lblAuthor.setText(street); // 123 Street Name
		
		lblSubjectLabel.setText(" ");
		lblSubject.setText(city + ", " + state); // City, State
		
		lblCategoryLabel.setText(" ");
		lblCategory.setText(zip); // Zipcode
		
		lblTotal.setText(String.format("$%.2f", b.price*b.quantity));
		
		lblTotalLabel.setVisible(true);
		lblTotal.setVisible(true);
	}
}
