package comp4410.booksRUs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

@SuppressWarnings("serial")
public class EditInventoryDialog extends JDialog
{
	private JComponent parent;
	
	private Product product;
	private Connection con;
	
	public EditInventoryDialog(JComponent parent, Product p, Connection con)
	{
		this.con = con;
		this.parent = parent;
		product = p;
		setupDialog();
		setTitle("Edit Inventory");
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setMinimumSize(new Dimension(520, 184));
		setPreferredSize(new Dimension(520, 184));
		setVisible(true);
	}

	private void setupDialog()
	{
		getContentPane().setLayout(new BorderLayout());
		JPanel dialogPane = new JPanel();
		
		JLabel lblPidLabel = new JLabel("Product ID:");
		JLabel lblPid = new JLabel(String.valueOf(product.productId));
		
		JLabel lblNameLabel = new JLabel("Product Name:");
		JLabel lblName = new JLabel(product.title);
		
		JLabel lblQuantityLabel = new JLabel("Current Inventory:");
		JLabel lblQuantity = new JLabel(String.valueOf(product.quantity));
		
		JLabel lblAdjust = new JLabel("Adjust inventory by:");
		JTextField tfAdjust = new JTextField(10);
		tfAdjust.setText("0");
		tfAdjust.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				tfAdjust.setBackground(Color.WHITE);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				tfAdjust.setBackground(Color.WHITE);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				tfAdjust.setBackground(Color.WHITE);
			}
			
		});
		
		JButton btnSave = new JButton("Save");
		btnSave.setPreferredSize(new Dimension(180,25));
		btnSave.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				
				try {
					int response = JOptionPane.showConfirmDialog(null, new JLabel("The new inventory quantity will be " + (product.quantity + Integer.parseInt(tfAdjust.getText())) + ".  Is this correct?",JLabel.CENTER), "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (response == JOptionPane.YES_OPTION)
					{
						
						int newQuantity = Math.max(product.quantity + Integer.parseInt(tfAdjust.getText()), 0);
						String query;
						Statement st;
						
						try 
						{
							query = "UPDATE products Set quantity = " + newQuantity + " WHERE productId=" + product.productId;
							st = con.createStatement();
							st.executeUpdate(query);
							product.quantity = newQuantity;
							dispose();
						} catch(SQLException e2) {
							e2.printStackTrace();
						}
					}
					else if (response == JOptionPane.CLOSED_OPTION)
					{
					      System.out.println("JOptionPane closed");
					}
				} catch(NumberFormatException e2) {
					tfAdjust.setText("0");
					tfAdjust.setBackground(Color.RED);
				}
			}
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setPreferredSize(new Dimension(180,25));
		btnCancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();
			}
		});
		
		GroupLayout layout = new GroupLayout(dialogPane);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		layout.setHorizontalGroup(
			layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup()
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblPidLabel)
								.addComponent(lblPid)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblNameLabel)
								.addComponent(lblName)
							)
						)
						.addGap(20, 20, 1000)
						.addGroup(layout.createParallelGroup()
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblQuantityLabel)
								.addComponent(lblQuantity)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblAdjust)
								.addComponent(tfAdjust)
							)
						)
					)
					.addGroup(layout.createSequentialGroup()
						.addGap(20)
						.addComponent(btnCancel)
						.addGap(10, 10, 1000)
						.addComponent(btnSave)
						.addGap(30)
					)
				)
		);

		layout.setVerticalGroup(
			layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addGap(10)
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblPidLabel)
								.addComponent(lblPid)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblNameLabel)
								.addComponent(lblName)
							)
						)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblQuantityLabel)
								.addComponent(lblQuantity)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblAdjust)
								.addComponent(tfAdjust)
							)
						)
					)
					.addGap(20, 20, 1000)
					.addGroup(layout.createParallelGroup()
						.addComponent(btnCancel)
						.addComponent(btnSave)
					)
				)
		);
		
		layout.linkSize(SwingConstants.HORIZONTAL, btnCancel, btnSave);
		layout.linkSize(SwingConstants.VERTICAL, btnCancel, btnSave, tfAdjust, lblAdjust, lblQuantityLabel, lblQuantity, lblNameLabel, lblName, lblPidLabel, lblPid);
		
		dialogPane.setLayout(layout);
		
		this.add(dialogPane);
		setSize(new Dimension(400,400));
		setLocationRelativeTo(parent);
		pack();
	}
}
