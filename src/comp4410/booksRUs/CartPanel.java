package comp4410.booksRUs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;


@SuppressWarnings("serial")
public class CartPanel extends JPanel
{
	public Color gray = new Color(60,60,60,255);
	private JPanel scrollingPanel;
	private ArrayList<CartItemPanel> items;
	private Connection con;
	private JLabel lblCartTotal;
	private JLabel lblShipping;
	private JLabel lblTotal;
	private MainFrame parent;
	private JButton btnCheckout;

	public CartPanel(Connection connection, MainFrame p) 
	{
		parent = p;
		this.setBackground(gray);

		con = connection;
		
		scrollingPanel = new JPanel();
		scrollingPanel.setBackground(gray);
		scrollingPanel.setLayout(new BoxLayout(scrollingPanel, BoxLayout.Y_AXIS));

		JScrollPane scroll = new JScrollPane(scrollingPanel);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.getVerticalScrollBar().setUnitIncrement(12);		

		JLabel lblCart = new JLabel("Cart:");
		lblCart.setPreferredSize(new Dimension(200,25));
		lblCart.setForeground(Color.white);

		
		JLabel lblCartTotalLabel = new JLabel("Cart Total:");
		lblCartTotal = new JLabel("$0.00");
		lblCartTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCartTotalLabel.setForeground(Color.white);
		lblCartTotal.setForeground(Color.white);
		
		JLabel lblShippingLabel = new JLabel("Shipping:");
		lblShipping = new JLabel("$0.00");
		lblShipping.setHorizontalAlignment(SwingConstants.RIGHT);
		lblShippingLabel.setForeground(Color.white);
		lblShipping.setForeground(Color.white);
		
		JLabel lblTotalLabel = new JLabel("Total:");
		lblTotal = new JLabel("$0.00");
		lblTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTotalLabel.setForeground(Color.white);
		lblTotal.setForeground(Color.white);

		JButton btnUpdateCart = new JButton("Update");
		btnUpdateCart.setPreferredSize(new Dimension(180,25));
		btnUpdateCart.setBackground(gray);
		btnUpdateCart.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateCart(true);
			}
		});
		
		btnCheckout = new JButton("Checkout");
		btnCheckout.setPreferredSize(new Dimension(180,25));
		btnCheckout.setBackground(gray);
		btnCheckout.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(items.size() > 0 && updateCart(true)) {
					String query;
					Statement st;
					ResultSet rs;
					
					try 
					{
						query = String.format("INSERT INTO ordertransactions (email, date) Values('%s', '%s')", parent.getEmail(), parent.getDate());
						st = con.createStatement();
						st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
						rs = st.getGeneratedKeys();
						rs.next();
						int ordernum = rs.getInt(1);
						for(CartItemPanel p : items) {
							query = String.format("INSERT INTO Partof VALUES(%d,%d,%d)", ordernum, p.pid, p.getQuantity());
							st.executeUpdate(query);
						}
						JOptionPane.showMessageDialog(parent, "Order Completed Successfully.");
						items.clear();
						updateScrollPanel();
						
					} catch(SQLException e2) {
						e2.printStackTrace();
					}
				}
			}
		});
		
		GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
		
        hGroup.addGap(10);
		hGroup.addGroup(layout.createParallelGroup()
			.addComponent(lblCart)
			.addComponent(scroll)
			.addGroup(layout.createSequentialGroup()
					.addComponent(lblCartTotalLabel)
					.addGap(50,50,100)
					.addComponent(lblCartTotal)
			)
			.addGroup(layout.createSequentialGroup()
					.addComponent(lblShippingLabel)
					.addGap(50,50,100)
					.addComponent(lblShipping)
			)
			.addGroup(layout.createSequentialGroup()
					.addComponent(lblTotalLabel)
					.addGap(50,50,100)
					.addComponent(lblTotal)
			)
			.addComponent(btnUpdateCart)
			.addComponent(btnCheckout)
		);
		
		hGroup.addGap(10);

		layout.setHorizontalGroup(hGroup);
		
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		
		vGroup.addGroup(layout.createSequentialGroup()
			.addComponent(lblCart)
			.addComponent(scroll)
			.addGap(20)
			.addGroup(layout.createParallelGroup()
					.addComponent(lblCartTotalLabel)
					.addComponent(lblCartTotal)
				)
			.addGroup(layout.createParallelGroup()
					.addComponent(lblShippingLabel)
					.addComponent(lblShipping)
				)
			.addGroup(layout.createParallelGroup()
					.addComponent(lblTotalLabel)
					.addComponent(lblTotal)
				)
			.addGap(20)
			.addComponent(btnUpdateCart)
			.addComponent(btnCheckout)
		);
      
        layout.linkSize(SwingConstants.HORIZONTAL, lblCart, scroll, btnUpdateCart, btnCheckout);
        layout.linkSize(SwingConstants.HORIZONTAL, lblCartTotalLabel, lblShippingLabel, lblTotalLabel,lblCartTotal,lblShipping,lblTotal);
        layout.linkSize(SwingConstants.VERTICAL, lblCart, btnUpdateCart, btnCheckout);

		layout.setVerticalGroup(vGroup);
		
		items = new ArrayList<CartItemPanel>();
		addResult(null);
		updateCart(false);
	}

	public void addResult(CartItemPanel newItem) 
	{
		boolean doit = true;
		if(newItem == null) {
			doit = false;
		} else {
			for(CartItemPanel p : items) {
				if(p.pid == newItem.pid) {
					doit = false;
				}
			}
		}
		
		if(doit) {
			items.add(newItem);
		} else {
			for(CartItemPanel p : items) {
				if(p.pid == newItem.pid) {
					p.setQuantity(p.getQuantity()+1);
					p.updateQuantity();
				}
			}
		}

		updateCart(false);
		updateScrollPanel();
		updateSize();
		updateTotals();
	}
	
	public void updateScrollPanel() {
		scrollingPanel.setLayout(new BoxLayout(scrollingPanel, BoxLayout.Y_AXIS));
		scrollingPanel.removeAll();
		int numResults = items.size();
		
		if(numResults == 0) {
			JLabel label = new JLabel("<HTML><U>Cart Empty</U></HTML");
			label.setForeground(Color.white);
			label.setHorizontalAlignment(SwingConstants.CENTER);
			scrollingPanel.setLayout(new GridLayout(numResults,1,1,0));
			scrollingPanel.add(label);
		}

		for(CartItemPanel i : items) 
		{
			scrollingPanel.add(i);
		}
		resetTotals();
		revalidate();
	}
	
	public void updateSize()
	{
		scrollingPanel.revalidate();
		scrollingPanel.repaint();
	}
	
	private boolean updateCart(boolean showMessages) {
		boolean ret = validateCart(showMessages);
		updateTotals();
		return ret;
	}

	void updateTotals() {
		double tot = 0;
		double ship = 0;
		for(CartItemPanel c : items) {
			tot += c.getTotal();
			ship += c.getShip();
		}
		lblCartTotal.setText(String.format("$%.2f", tot));
		lblShipping.setText(String.format("$%.2f", ship));
		lblTotal.setText(String.format("$%.2f", tot+ship));
		if(tot >= 50) {
			ship = 0;
			lblShipping.setText(String.format("Free!", ship));
		}
	}

	private boolean validateCart(boolean showMessages) {
		boolean readyToGo = true;
		boolean quantGood = true;
		for(int i = 0; i < items.size(); i++) {
			if(!items.get(i).setQuantity()) {
				readyToGo = false;
			}
			if(items.get(i).getQuantity() == 0) {
				items.remove(i);
				updateScrollPanel();
				updateSize();
				updateTotals();
			}
		}
		for(CartItemPanel c : items) {
			quantGood = quantGood ? c.validateQuantity(con) : quantGood;
		}
		if(!quantGood) {
			readyToGo = false;
		}
		if(items.size() == 0) {
			btnCheckout.setEnabled(false);
			readyToGo = false;
		} else {
			btnCheckout.setEnabled(true);
		}
		if(!readyToGo && showMessages) {
			JOptionPane.showMessageDialog(parent, "There were issues with your cart that prevented a successful checkout. Your cart has been updated accordingly.");
		}
		return readyToGo;
	}
	
	public void resetTotals() {
		lblCartTotal.setText("$0.00");
		lblShipping.setText("$0.00");
		lblTotal.setText("$0.00");
	}
}
