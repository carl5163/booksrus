package comp4410.booksRUs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class AdminPanel extends JPanel
{
	
	public static String[] MONTHS = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	public static String[] DAYS;
	public static String[] YEARS = {"2013", "2014", "2015", "2016"};
	
	public Color gray = new Color(60,60,60,255);
	private JButton btnRecentlyPurchasedProducts;
	private Connection con;
	private MainFrame parent;
	
	public AdminPanel(MainFrame parent, Connection connection) 
	{
		this.parent = parent;
		setBackground(gray);
		con = connection;		
	
		DAYS = new String[31];
		for(int i = 1; i <= 31; i++) {
			DAYS[i-1] = Integer.toString(i); 
		}
										
		Calendar yesterday = Calendar.getInstance(); 
		yesterday.setTimeInMillis(System.currentTimeMillis());
		yesterday.add(Calendar.DAY_OF_MONTH, -1);
		
		JLabel lblOther = new JLabel("Purchases in the Past 24:");
		lblOther.setForeground(Color.white);	
		JLabel lblTopTen = new JLabel("Top Ten Purchases in the Past 7 Days:");
		lblTopTen.setForeground(Color.white);
		
		btnRecentlyPurchasedProducts = new JButton("Go");
		btnRecentlyPurchasedProducts.setPreferredSize(new Dimension(180,25));
		btnRecentlyPurchasedProducts.setBackground(gray);
		btnRecentlyPurchasedProducts.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{

				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				ArrayList<ResultPanel> results = new ArrayList<ResultPanel>();
				String query;
				Statement st;
				ResultSet rs;
				try
				{
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN PartOf NATURAL JOIN Authors NATURAL JOIN Books NATURAL JOIN Products NATURAL JOIN Publishers", parent.userEmail);
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						Calendar date = Calendar.getInstance();
						date.setTime(formatter.parse(rs.getString(rs.findColumn("date"))));						
						if(date.compareTo(yesterday) >= 0) {
							Book b = new Book(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("ISBN")),
								rs.getString(rs.findColumn("subject")),
								rs.getString(rs.findColumn("category")),
								rs.getString(rs.findColumn("pubName")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantityOrdered")),
								String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
								rs.getInt(rs.findColumn("year")),
								rs.getString(rs.findColumn("name")));
							String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
							String phone = rs.getString(rs.findColumn("phone"));
							String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
							results.add(new OrderedBookPanel(null, b, rs.getString(rs.findColumn("date")), pubAddress, phone, authAddress, true, con, rs.getString(rs.findColumn("orderNum"))));
						}
					}
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN PartOf NATURAL JOIN DVDs NATURAL JOIN Products", parent.userEmail);
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						Calendar date = Calendar.getInstance();
						date.setTime(formatter.parse(rs.getString(rs.findColumn("date"))));	
						if(date.compareTo(yesterday) >= 0) {
							DVD d = new DVD(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("director")),
								rs.getString(rs.findColumn("genre")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantityOrdered")),
								String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
								rs.getInt(rs.findColumn("year")));
							results.add(new OrderedDVDPanel(parent, null, d, rs.getString(rs.findColumn("date")), true, con, rs.getString(rs.findColumn("orderNum"))));	
						}
					}
				}
				catch(SQLException ex)
				{
					ex.printStackTrace();
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
				parent.addResultsToTable(results);
			}
		});
		
		JButton btnTopTen = new JButton("Top Ten Products");
		btnTopTen.setPreferredSize(new Dimension(180,25));
		btnTopTen.setBackground(gray);
		btnTopTen.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try {
					parent.addResultsToTable(getTopTen(con));
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		JButton btnAddUser = new JButton("Add Customer");
		btnAddUser.setPreferredSize(new Dimension(180,25));
		btnAddUser.setBackground(gray);
		btnAddUser.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				new AddCustomerDialog(null, con);
			}
		});
		
		JButton btnAddItem = new JButton("Add Item");
		btnAddItem.setPreferredSize(new Dimension(180,25));
		btnAddItem.setBackground(gray);
		btnAddItem.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				new AddItemDialog(null, con);
			}
		});
		
		GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        
        hGroup.addGap(10);
		hGroup.addGroup(layout.createParallelGroup()
			.addComponent(lblOther)
			.addComponent(btnRecentlyPurchasedProducts)
			.addComponent(lblTopTen)
			.addComponent(btnTopTen)
			
			.addComponent(btnAddUser)
			.addComponent(btnAddItem)
		);
		
		hGroup.addGap(10);

		layout.setHorizontalGroup(hGroup);
		
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		
		vGroup.addGroup(layout.createSequentialGroup()
			.addComponent(lblOther)
			.addGap(10)
			.addComponent(btnRecentlyPurchasedProducts)
			.addGap(30)
			.addComponent(lblTopTen)
			.addComponent(btnTopTen)
			.addGap(50)
			.addComponent(btnAddUser)
			.addComponent(btnAddItem)
		);
		layout.linkSize(SwingConstants.HORIZONTAL, btnTopTen, btnRecentlyPurchasedProducts, btnAddUser, btnAddItem);
		layout.setVerticalGroup(vGroup);
	}

	public ArrayList<ResultPanel> getTopTen(Connection con) throws ParseException 
	{
		ArrayList<ResultPanel> results = new ArrayList<ResultPanel>();

		String query;
		Statement st;
		ResultSet rs;
		
		try 
		{
			int[] topTen = getTopTenArray(con);
			
			
			for(int i = 0; i < 10; i++) {
				query = "Select DISTINCT * From Partof NATURAL JOIN Books NATURAL JOIN Authors NATURAL JOIN Products NATURAL JOIN Publishers WHERE productid = " + topTen[i];
				st = con.createStatement();
				rs = st.executeQuery(query);
				if(rs.next()) 
				{
					Book b = new Book(
						rs.getInt(rs.findColumn("productId")),
						rs.getString(rs.findColumn("ISBN")),
						rs.getString(rs.findColumn("subject")),
						rs.getString(rs.findColumn("category")),
						rs.getString(rs.findColumn("pubName")),
						rs.getDouble(rs.findColumn("price")),
						rs.getInt(rs.findColumn("quantity")),
						String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
						rs.getInt(rs.findColumn("year")),
						rs.getString(rs.findColumn("name")));
					String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
					String phone = rs.getString(rs.findColumn("phone"));
					String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
					results.add(new TopTenBookPanel(null, b, i+1, pubAddress, phone, authAddress, true, con));
				}
			}
			
			for(int i = 0; i < 10; i++) {
				query = "Select DISTINCT * From Partof NATURAL JOIN Dvds NATURAL JOIN Products WHERE productid = " + topTen[i];
				st = con.createStatement();
				rs = st.executeQuery(query);
				if(rs.next()) 
				{
					DVD d = new DVD(
						rs.getInt(rs.findColumn("productId")),
						rs.getString(rs.findColumn("director")),
						rs.getString(rs.findColumn("genre")),
						rs.getDouble(rs.findColumn("price")),
						rs.getInt(rs.findColumn("quantity")),
						String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
						rs.getInt(rs.findColumn("year")));
					results.add(new TopTenDVDPanel(parent, null, d, i+1, true, con));	
				}
			}
		} 
		catch(SQLException e) 
		{
			e.printStackTrace();
		}
		
		ArrayList<ResultPanel> ret = new ArrayList<ResultPanel>();

		for(int i = 0; i < 10; i++) {
			for(ResultPanel p : results) {
				if(p instanceof TopTenBookPanel) {
					if(((TopTenBookPanel)p).rank == i+1) {
						ret.add(p);
					}
				} 
				else 
				{
					if(((TopTenDVDPanel)p).rank == i+1) {
						ret.add(p);
					}
				}
			}
		}

		return ret;
	}

	public static int[] getTopTenArray(Connection con) throws ParseException {

		String query;
		Statement st;
		ResultSet rs;
		int[] topTen = new int[10];
		
		try 
		{

			Calendar lastWeek = Calendar.getInstance(); 
			lastWeek.setTimeInMillis(System.currentTimeMillis());
			lastWeek.add(Calendar.DAY_OF_MONTH, -7);
			
			Map<Integer, Integer> rankMap = new HashMap<Integer, Integer>();
			
			query = "Select DISTINCT * From Partof NATURAL JOIN Books NATURAL JOIN Products NATURAL JOIN OrderTransactions";
			st = con.createStatement();
			rs = st.executeQuery(query);
			while(rs.next()) 
			{
				int pid = rs.getInt(rs.findColumn("productId"));
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				Calendar date = Calendar.getInstance();
				date.setTime(formatter.parse(rs.getString(rs.findColumn("date"))));
				
				if(date.compareTo(lastWeek) >= 0)
				{
					if(rankMap.containsKey(pid))
					{
						rankMap.put(pid, rankMap.get(pid) + rs.getInt(rs.findColumn("quantityOrdered")));
					} 
					else 
					{
						rankMap.put(pid, rs.getInt(rs.findColumn("quantityOrdered")));
					}
				}
			}
			
			query = "Select DISTINCT * From Partof NATURAL JOIN DVDs NATURAL JOIN Products NATURAL JOIN OrderTransactions";
			st = con.createStatement();
			rs = st.executeQuery(query);			
			while(rs.next()) 
			{
				int pid = rs.getInt(rs.findColumn("productId"));
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				Calendar date = Calendar.getInstance();
				date.setTime(formatter.parse(rs.getString(rs.findColumn("date"))));
				
				if(date.compareTo(lastWeek) >= 0)
				{
					if(rankMap.containsKey(pid))
					{
						rankMap.put(pid, rankMap.get(pid) + rs.getInt(rs.findColumn("quantityOrdered")));
					} 
					else 
					{
						rankMap.put(pid, rs.getInt(rs.findColumn("quantityOrdered")));
					}
				}
			}
			
			int rank = 0;
			for(int i : rankMap.keySet()) {
				{
					while(rank < 10)
					{
						if(rankMap.get(topTen[rank]) == null)
						{
							break;
						}
						if(rankMap.get(i) >= rankMap.get(topTen[rank]))
						{
							break;
						}
						rank++;
					}
					if(rank < 10) 
					{
						for(int r = 9; r > rank; r--) 
						{
							topTen[r] = topTen[r-1];
						}
						topTen[rank] = i;
					}
					rank = 0;
				}
			}
		} 
		catch(SQLException e) 
		{
			e.printStackTrace();
		}

		return topTen;
	}
}
