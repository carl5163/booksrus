package comp4410.booksRUs;

public class Book extends Product
{
	
	String isbn;
	String subject;
	String category;
	String pubName;
	String author;
	
	public Book(int pid, String isn, String sub, String cat, String pub, double p, int q, String t, int y, String a)
	{
		super(pid, p, q, t, y);
		isbn = isn;
		subject = sub;
		category = cat;
		pubName = pub;
		author = a;
	}
}
