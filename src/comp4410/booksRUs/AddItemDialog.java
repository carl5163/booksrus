package comp4410.booksRUs;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class AddItemDialog extends JDialog
{
	private JComponent parent;
	
	// Product
	int productId;
	double price;
	int quantity;
	String title;
	int year;
	
	// Book
	String isbn;
	String subject;
	String category;
	String pubName;
	String author;

	String director;
	String genre;
	
	Connection con;
	
	public AddItemDialog(JComponent parent, Connection con)
	{
		this.parent = parent;
		this.con = con;
		
		setTitle("Add Item");
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		Object[] options = {"Book",
		                    "DVD",
		                    "Cancel"};
		int n = JOptionPane.showOptionDialog(null,
		    "Which product do you want to add?",
		    "Add Item",
		    JOptionPane.YES_NO_CANCEL_OPTION,
		    JOptionPane.PLAIN_MESSAGE,
		    null,
		    options,
		    options[2]);
		
		if(n == 0)
		{
			//book
			setupDialog(n);
			setVisible(true);
		}
		else if(n == 1)
		{
			//dvd
			setupDialog(n);
			setVisible(true);
		}
		else
		{
			this.dispose();
		}
		
		
	}
	
	private void setupDialog(int t)
	{
		getContentPane().setLayout(new BorderLayout());
		JPanel dialogPane = new JPanel();
		
		JLabel lblTitle = new JLabel("Title:");
		JTextField tfTitle = new JTextField();
		tfTitle.setPreferredSize(new Dimension(180,25));
		
		JLabel lblYear = new JLabel("Year:");
		JTextField tfYear = new JTextField();
		
		JLabel lblISBN = new JLabel("ISBN:");
		JTextField tfISBN = new JTextField();
		
		JLabel lblSubject = new JLabel("Subject:");
		JTextField tfSubject = new JTextField();
		
		JLabel lblCat = new JLabel("Category:");
		JTextField tfCat = new JTextField();
		
		JLabel lblGenre = new JLabel("Genre:");
		JTextField tfGenre = new JTextField();
		
		JLabel lblPub = new JLabel("Publisher:");
		JTextField tfPub = new JTextField();
		
		JLabel lblAuth = new JLabel("Author:");
		JTextField tfAuth = new JTextField();
		
		JLabel lblDir = new JLabel("Director:");
		JTextField tfDir = new JTextField();
		
		JLabel lblPrice = new JLabel("Price:");
		JTextField tfPrice = new JTextField();
		
		JLabel lblQuant = new JLabel("Quantity:");
		JTextField tfQuant = new JTextField();
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(t == 0)
				{
					boolean readyToAdd = true;
					int authID = 0;
					
					// Book
					if(!pubExist(tfPub.getText().trim()))
					{
						readyToAdd = false;
						
						JTextField name = new JTextField();
						JTextField address = new JTextField();
						JTextField city = new JTextField();
						JTextField state = new JTextField();
						JTextField zip = new JTextField();
						JTextField url = new JTextField();
						
						Object[] message = {
						    "Name:", name,
						    "Adress:", address,
						    "City:", city,
						    "State:", state,
						    "Zip:", zip,
						    "URL:", url
						};
						
						int option = JOptionPane.showConfirmDialog(null, message, "Add Publisher", JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);
						if (option == JOptionPane.OK_OPTION)
						{
							addPublisher(name.getText().trim(), url.getText().trim(), address.getText().trim(), city.getText().trim(), state.getText().trim(), zip.getText().trim());
							readyToAdd = true;
						}
						else
						{
							readyToAdd = false;
						}
					}
					
					if(!authorExist(tfAuth.getText().trim()))
					{
						readyToAdd = false;
						JTextField name = new JTextField();
						JTextField address = new JTextField();
						JTextField city = new JTextField();
						JTextField state = new JTextField();
						JTextField zip = new JTextField();
						
						
						
						Object[] message = {
						    "Name:", name,
						    "Adress:", address,
						    "City:", city,
						    "State:", state,
						    "Zip:", zip
						};
						
						int option = JOptionPane.showConfirmDialog(null, message, "Add Author", JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);
						if (option == JOptionPane.OK_OPTION)
						{
							authID = addAuthor(name.getText().trim(), address.getText().trim(), city.getText().trim(), state.getText().trim(), zip.getText().trim());
							if(authID < 0)
							{
								readyToAdd = false;
								System.out.println("UH OH");
							}
							else
								readyToAdd = true;
						}
						else
						{
							readyToAdd = false;
						}
					}
					
					if(readyToAdd)
					{
						addBook(Double.valueOf(tfPrice.getText().trim()), Integer.valueOf(tfQuant.getText().trim()), tfTitle.getText().trim(), Integer.valueOf(tfYear.getText().trim()), tfISBN.getText().trim(), tfSubject.getText().trim(), tfCat.getText().trim(), Integer.valueOf(authID), tfPub.getText().trim());
					}
					else
					{}
				}
				else
				{
					addDVD(Double.valueOf(tfPrice.getText().trim()), Integer.valueOf(tfQuant.getText().trim()), tfTitle.getText().trim(), Integer.valueOf(tfYear.getText().trim()), tfDir.getText().trim(), tfGenre.getText().trim());
				}
			}
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();
			}
		});
		
		GroupLayout layout = new GroupLayout(dialogPane);
		
		layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
		
		dialogPane.setLayout(layout);

        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

        if(t == 0)
        {
	        hGroup.addGroup(layout.createParallelGroup()
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblTitle)
					.addComponent(tfTitle)
				)
        		.addGroup(layout.createSequentialGroup()
					.addComponent(lblYear)
					.addComponent(tfYear)
				)
        		.addGroup(layout.createSequentialGroup()
					.addComponent(lblISBN)
					.addComponent(tfISBN)
				)
        		.addGroup(layout.createSequentialGroup()
					.addComponent(lblSubject)
					.addComponent(tfSubject)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblCat)
					.addComponent(tfCat)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblPub)
					.addComponent(tfPub)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblAuth)
					.addComponent(tfAuth)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblPrice)
					.addComponent(tfPrice)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblQuant)
					.addComponent(tfQuant)
				)
	        	.addGroup(layout.createSequentialGroup()
						.addGap(40)
						.addComponent(btnCancel)
						.addGap(10, 10, 1000)
						.addComponent(btnSave)
						.addGap(50)
					)
			);
        }
        else
        {
	        hGroup.addGroup(layout.createParallelGroup()
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblTitle)
					.addComponent(tfTitle)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblYear)
					.addComponent(tfYear)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblGenre)
					.addComponent(tfGenre)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblDir)
					.addComponent(tfDir)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblPrice)
					.addComponent(tfPrice)
				)
	        	.addGroup(layout.createSequentialGroup()
					.addComponent(lblQuant)
					.addComponent(tfQuant)
				)
	        	.addGroup(layout.createSequentialGroup()
						.addGap(40)
						.addComponent(btnCancel)
						.addGap(10, 10, 1000)
						.addComponent(btnSave)
						.addGap(50)
					)
			);
        }
        
		layout.setHorizontalGroup(hGroup);
		
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		
		if(t == 0)
		{
			vGroup.addGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(lblTitle)
					.addComponent(tfTitle)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblYear)
					.addComponent(tfYear)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblISBN)
					.addComponent(tfISBN)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblSubject)
					.addComponent(tfSubject)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblCat)
					.addComponent(tfCat)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblPub)
					.addComponent(tfPub)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblAuth)
					.addComponent(tfAuth)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblPrice)
					.addComponent(tfPrice)
				)
	        	.addGroup(layout.createParallelGroup()
					.addComponent(lblQuant)
					.addComponent(tfQuant)
				)
	        	.addGap(20, 20, 1000)
				.addGroup(layout.createParallelGroup()
					.addComponent(btnCancel)
					.addComponent(btnSave)
				)
			);
		}
		else
		{
			vGroup.addGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(lblTitle)
					.addComponent(tfTitle)
				)
        		.addGroup(layout.createParallelGroup()
					.addComponent(lblYear)
					.addComponent(tfYear)
				)
        		.addGroup(layout.createParallelGroup()
      				.addComponent(lblGenre)
					.addComponent(tfGenre)
				)
        		.addGroup(layout.createParallelGroup()
       				.addComponent(lblDir)
					.addComponent(tfDir)
				)
        		.addGroup(layout.createParallelGroup()
					.addComponent(lblPrice)
					.addComponent(tfPrice)
				)
        		.addGroup(layout.createParallelGroup()
					.addComponent(lblQuant)
					.addComponent(tfQuant)
				)
        		.addGap(20, 20, 1000)
				.addGroup(layout.createParallelGroup()
					.addComponent(btnCancel)
					.addComponent(btnSave)
				)
			);
		}
		layout.setVerticalGroup(vGroup);
		
		if(t == 0)
		{
			layout.linkSize(SwingConstants.HORIZONTAL, lblTitle, lblYear, lblISBN, lblSubject, lblCat, lblPub, lblAuth, lblPrice, lblQuant);
			layout.linkSize(SwingConstants.HORIZONTAL, tfTitle, tfYear, tfISBN, tfSubject, tfCat, tfPub, tfAuth, tfPrice, tfQuant);
			
			layout.linkSize(SwingConstants.VERTICAL, lblTitle, lblYear,tfSubject,tfCat,tfPub,tfAuth,tfPrice,tfQuant, lblISBN, lblSubject, lblCat, lblPub, lblAuth, lblPrice, lblQuant, tfTitle,tfYear,tfISBN);
		}
		else
		{
			layout.linkSize(SwingConstants.HORIZONTAL, lblTitle, lblYear, lblGenre, lblDir, lblPrice, lblQuant);
			layout.linkSize(SwingConstants.HORIZONTAL, tfTitle, tfYear, tfGenre, tfDir, tfPrice, tfQuant);
			layout.linkSize(SwingConstants.VERTICAL, lblTitle, lblYear,tfGenre,tfDir,tfPrice,tfQuant, lblPrice, lblQuant, tfTitle,tfYear);
		}
		
		layout.linkSize(SwingConstants.HORIZONTAL, btnCancel, btnSave);
		
		this.add(dialogPane);
		
		if(t==0)
			setMinimumSize(new Dimension(268,377));
		else
			setMinimumSize(new Dimension(262,284));
		
		pack();
		
		setLocationRelativeTo(parent);
	}
	
	public void addDVD(double price, int quantity, String title, int year, String director, String genre) {
		String query;
		Statement st;
		ResultSet rs;		
		try 
		{
			query = String.format("INSERT INTO products (price, quantity, title, year) Values(%.2f, %d, '%s', %d)", price, quantity, title, year);
			st = con.createStatement();
			st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			rs = st.getGeneratedKeys();
			rs.next();
			int pid = rs.getInt(1);
			query = String.format("INSERT INTO dvds Values(%d, '%s', '%s')", pid, director, genre);
			st.executeUpdate(query);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addBook(double price, int quantity, String title, int year, String isbn, String subject, String category, int authorId, String pubName) {
		String query;
		Statement st;
		ResultSet rs;		
		try 
		{
			query = String.format("INSERT INTO products (price, quantity, title, year) Values(%.2f, %d, '%s', %d)", price, quantity, title, year);
			st = con.createStatement();
			st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			rs = st.getGeneratedKeys();
			rs.next();
			int pid = rs.getInt(1);
			query = String.format("INSERT INTO books Values(%d, '%s', '%s', '%s', %d, '%s')", pid, isbn, subject, category, authorId, pubName);
			st.executeUpdate(query);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int addAuthor(String name, String address, String city, String state, String zip) {
		String query;
		Statement st;
		ResultSet rs;
		try 
		{
			query = String.format("INSERT INTO authors (name, authaddress, authcity, authstate, authzip) Values('%s', '%s', '%s', '%s', '%s')", name, address, city, state, zip);
			st = con.createStatement();
			st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			rs = st.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return -1;
		
	}
	
	public void addPublisher(String name, String url, String address, String city, String state, String zip) {
		String query;
		Statement st; 
		try 
		{
			query = String.format("INSERT INTO publishers (pubname, url, address, city, state, zip) Values('%s', '%s', '%s', '%s', '%s', '%s')", name, url, address, city, state, zip);
		    st = con.createStatement();
		    st.executeUpdate(query);
		} catch(SQLException e) {
		    e.printStackTrace();
		}
	}
	
	public boolean authorExist(String name) {
		String query;
		Statement st;	
		ResultSet rs;
		try 
		{
			query = String.format("SELECT * FROM Authors WHERE name='%s'", name);
			st = con.createStatement();
			rs = st.executeQuery(query);
			if(rs.next()) {
				return true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean pubExist(String name) {
		String query;
		Statement st;	
		ResultSet rs;
		try 
		{
			query = String.format("SELECT * FROM Publishers WHERE pubName='%s'", name);
			st = con.createStatement();
			rs = st.executeQuery(query);
			if(rs.next()) {
				return true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
