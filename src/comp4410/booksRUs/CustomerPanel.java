package comp4410.booksRUs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.CompoundBorder;

@SuppressWarnings("serial")
public class CustomerPanel  extends ResultPanel
{
	private Customer customer;

	JLabel iconLabel;
	
	JButton dynamicButton;
	Connection con;
	MainFrame parent;
	
	public Color gray = new Color(60,60,60,255);
	
	public CustomerPanel(MainFrame parent, Customer c, Connection con) 
	{
		customer = c;
		this.con = con;
		this.parent = parent;
		setupPanel();
	}

	private void setupPanel()
	{
		removeAll();
		JLabel lblCustomerNameLabel = new JLabel("Name:");		
		JLabel lblCustomerName = new JLabel(customer.name);
		
		JLabel lblCustomerEmailLabel = new JLabel("Email:");		
		JLabel lblCustomerEmail = new JLabel(customer.email);
		
		JLabel lblCustomerPhoneLabel = new JLabel("Phone:");		
		JLabel lblCustomerPhone = new JLabel(customer.phone);
		
		JLabel lblCustomerAddressLabel = new JLabel("Address:");		
		JLabel lblCustomerAddress = new JLabel(String.format("<HTML>%s</HTML>", customer.address));
		
		JButton btnOrdersCustomer = new JButton("Orders");
		
		btnOrdersCustomer.setPreferredSize(new Dimension(84,84));
		btnOrdersCustomer.setMinimumSize(new Dimension(84,84));
		btnOrdersCustomer.setMaximumSize(new Dimension(84,84));
		btnOrdersCustomer.setBackground(gray);
		btnOrdersCustomer.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<ResultPanel> results = new ArrayList<ResultPanel>();
				String query;
				Statement st;
				ResultSet rs;
				try
				{
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN Partof NATURAL JOIN Books NATURAL JOIN Products NATURAL JOIN Authors NATURAL JOIN Publishers WHERE EMAIL = '%s'", customer.email);
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						Book b = new Book(
							rs.getInt(rs.findColumn("productId")),
							rs.getString(rs.findColumn("ISBN")),
							rs.getString(rs.findColumn("subject")),
							rs.getString(rs.findColumn("category")),
							rs.getString(rs.findColumn("pubName")),
							rs.getDouble(rs.findColumn("price")),
							rs.getInt(rs.findColumn("quantityOrdered")),
							String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
							rs.getInt(rs.findColumn("year")),
							rs.getString(rs.findColumn("name")));	
						String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
						String phone = rs.getString(rs.findColumn("phone"));
						String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
						results.add(new OrderedBookPanel(null, b, rs.getString(rs.findColumn("date")), pubAddress, phone, authAddress, true, con, rs.getString(rs.findColumn("ordernum"))));	
					}
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN Partof NATURAL JOIN DvDs NATURAL JOIN Products WHERE EMAIL = '%s'", customer.email);
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						DVD d = new DVD(
							rs.getInt(rs.findColumn("productId")),
							rs.getString(rs.findColumn("director")),
							rs.getString(rs.findColumn("genre")),
							rs.getDouble(rs.findColumn("price")),
							rs.getInt(rs.findColumn("quantityOrdered")),
							String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
							rs.getInt(rs.findColumn("year")));
						results.add(new OrderedDVDPanel(parent, null, d, rs.getString(rs.findColumn("date")), true, con, rs.getString(rs.findColumn("orderNum"))));	
					}
				}
				catch(SQLException ex)
				{
					ex.printStackTrace();
				}
				parent.addResultsToTable(results);
			
			}
		});
		
		JButton btnEditCustomer = new JButton("Edit");
		
		btnEditCustomer.setPreferredSize(new Dimension(84,84));
		btnEditCustomer.setMinimumSize(new Dimension(84,84));
		btnEditCustomer.setMaximumSize(new Dimension(84,84));
		btnEditCustomer.setBackground(gray);
		btnEditCustomer.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EditCustomerDialog(customer, con, null, false);
				setupPanel();
			}
		});
		
		JButton btnDeleteCustomer = new JButton("Delete");
		
		btnDeleteCustomer.setPreferredSize(new Dimension(84,84));
		btnDeleteCustomer.setMinimumSize(new Dimension(84,84));
		btnDeleteCustomer.setMaximumSize(new Dimension(84,84));
		btnDeleteCustomer.setBackground(gray);
		btnDeleteCustomer.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Delete " + customer.name + " from database? \n This will remove all of thier data!!", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.NO_OPTION)
				{}
				else if (response == JOptionPane.YES_OPTION)
				{
					String query;
					Statement st;
					
					try 
					{
						query = String.format("DELETE FROM customers WHERE email = '%s'", customer.email);
						st = con.createStatement();
						st.executeUpdate(query);	
						setVisible(false);
					} catch(SQLException e2) {
						e2.printStackTrace();
					}
				}
				else if (response == JOptionPane.CLOSED_OPTION)
				{}
			}
		});
		
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		layout.setHorizontalGroup(
			layout.createSequentialGroup()
			.addGap(120)
			.addGroup(layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addGap(20)
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblCustomerNameLabel)
							.addComponent(lblCustomerName)
						)
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblCustomerEmailLabel)
							.addComponent(lblCustomerEmail)
						)
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblCustomerPhoneLabel)
							.addComponent(lblCustomerPhone)
						)
					)
					.addGap(10, 100, 1000)
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblCustomerAddressLabel)
							.addComponent(lblCustomerAddress)
						)
					)
				)
			)
			.addGap(10)
			.addComponent(btnOrdersCustomer)
			.addGap(10)
			.addComponent(btnEditCustomer)
			.addGap(10)
			.addComponent(btnDeleteCustomer)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup()
			.addGroup(layout.createSequentialGroup()
				//.addComponent(lblTitle)
				.addGroup(layout.createParallelGroup()
					.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup()
							.addComponent(lblCustomerNameLabel)
							.addComponent(lblCustomerName)
						)
						.addGroup(layout.createParallelGroup()
							.addComponent(lblCustomerEmailLabel)
							.addComponent(lblCustomerEmail)
						)
						.addGroup(layout.createParallelGroup()
							.addComponent(lblCustomerPhoneLabel)
							.addComponent(lblCustomerPhone)
						)
					)
					.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup()
							.addComponent(lblCustomerAddressLabel)
							.addComponent(lblCustomerAddress)
						)
					)
				)
			)
			.addComponent(btnOrdersCustomer)
			.addComponent(btnEditCustomer)
			.addComponent(btnDeleteCustomer)
		);

		setLayout(layout);
		
		ImageIcon background = new ImageIcon(getClass().getResource("user.png"));
		iconLabel = new JLabel();
		iconLabel.setBounds(5, 5, background.getIconWidth(), background.getIconHeight());
		iconLabel.setIcon(background);
		add(iconLabel);
		
		setBorder(new CompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white), BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black)));
	}
}
