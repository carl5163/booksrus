package comp4410.booksRUs;

import javax.swing.*;

public class BooksRUs
{
	
	private static MainFrame frame;
	
	public static void main(String[] args)
	{
		start();
	}	
	
	public static void restart() {
		frame.dispose();
		start();
		
	}
	
	public static void start() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable() 
		{
			@Override
			public void run() 
			{
				frame = new MainFrame();
			}			
		});
	}
}
