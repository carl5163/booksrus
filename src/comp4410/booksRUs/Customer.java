package comp4410.booksRUs;

public class Customer {

	String name;
	String email;
	String phone;
	String address;	
	String street;
	String city;
	String state;
	String zip;
	
	public Customer(String name, String email, String phone, String street, String city, String state, String zip) {
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		address = getAddress();
	}

	public String getAddress() {
		return String.format("%s<br>%s, %s, %s", street, city, state, zip);
	}

}