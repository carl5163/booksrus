package comp4410.booksRUs;

public class DVD extends Product{
	
	String director;
	String genre;
	
	public DVD(int pid, String dir, String gen, double p, int q, String t, int y)
	{
		super(pid, p, q, t, y);
		director = dir;
		genre = gen;
	}
}
