package comp4410.booksRUs;

import java.sql.Connection;
import java.sql.SQLException;

@SuppressWarnings("serial")
public class OrderedDVDPanel extends DVDPanel
{
	double total;
	
	public OrderedDVDPanel(MainFrame parent, CartPanel p, DVD d, String date, boolean admin, Connection con, String orderNum) throws SQLException 
	{
		super(parent, p, d, admin, con, true, orderNum);

		total = d.price*d.quantity;	
		lblQuantityInStockLabel.setText("Quantity Ordered:");
		lblPrice.setText(String.format("$%.2f", d.price));
		lblYearLabel.setText("Date Ordered:");
		lblYear.setText(date);
		lblGenreLabel.setText("Order Number: ");
		lblGenre.setText(orderNum);
			
		
		lblTotal.setText(String.format("$%.2f", total));

		lblTotalLabel.setVisible(true);
		lblTotal.setVisible(true);
	}
}
