package comp4410.booksRUs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.sql.Connection;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class TopTenDVDPanel extends DVDPanel
{
	int rank;
	
	Color transBlack = new Color(0,0,0,180);

	public TopTenDVDPanel(MainFrame parent, CartPanel p, DVD d, int rank, boolean admin, Connection con) 
	{
		super(parent, p, d, admin, con);
		this.rank = rank;
		
		JLabel label = new JLabel();
		Font font = new Font("Tahoma", Font.BOLD, 60);
		
		label.setForeground(transBlack);

		label.setFont(font);
		
		label.setAlignmentY(JLabel.CENTER);
		label.setAlignmentX(JLabel.CENTER);
		
		AffineTransform affinetransform = new AffineTransform();     
		FontRenderContext frc = new FontRenderContext(affinetransform,true,true);     
		
		int textwidth = (int)(font.getStringBounds(Integer.toString(rank), frc).getWidth());
		int textheight = (int)(font.getStringBounds(Integer.toString(rank), frc).getHeight());
		
		label.setSize(new Dimension(textwidth,textheight));
		label.setText(Integer.toString(rank));

		label.setBounds(iconLabel.getWidth()/2 - textwidth/2, iconLabel.getHeight()/2 - textheight/2, textwidth, textheight);
		
		iconLabel.add(label);
	}
}
