package comp4410.booksRUs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.*;
import javax.swing.border.*;

@SuppressWarnings("serial")
public class BookPanel extends ResultPanel
{

	private Book book;
	JLabel lblQuantityInStockLabel;
	JLabel lblPriceLabel;
	JLabel lblPrice;
	JLabel lblYearLabel;
	JLabel lblYear;
	
	JLabel lblISBNLabel;
	JLabel lblISBN;
	JLabel lblSubjectLabel;
	JLabel lblSubject;
	JLabel lblCategoryLabel;
	JLabel lblCategory;
	JLabel lblAuthorLabel;
	JLabel lblAuthor;
	JLabel lblPublisherLabel;
	JLabel lblPublisher;
	
	JLabel iconLabel;
	JButton btnPub;
	JButton btnAuth;
	
	JLabel lblTotalLabel;
	JLabel lblTotal;
	
	String pubAddress;
	String pubPhone;
	String authAddress;
	boolean admin;
	JLabel lblQuantityInStock;
	
	JButton dynamicButton;
	
	public Color gray = new Color(60,60,60,255);
	CartPanel cart;
	Connection con;
	
	boolean isHistory = false;
	
		
	public BookPanel(CartPanel p, Book b, String pubAddress, String pubPhone, String authAddress, boolean admin, Connection con) 
	{
		this.con = con;
		cart = p;
		book = b;
		this.admin = admin;
		this.pubAddress = pubAddress;
		this.pubPhone = pubPhone;
		this.authAddress = authAddress;
		setupPanel();
	}
	
	public BookPanel(CartPanel p, Book b, String pubAddress, String pubPhone, String authAddress, boolean admin, Connection con, boolean h) 
	{
		this.con = con;
		cart = p;
		book = b;
		this.admin = admin;
		this.pubAddress = pubAddress;
		this.pubPhone = pubPhone;
		this.authAddress = authAddress;
		this.isHistory = h;
		setupPanel();
	}

	private void setupPanel()
	{
		JLabel lblTitle = new JLabel(String.format("<HTML><U>%s</U></HTML>", book.title));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		lblISBNLabel = new JLabel("ISBN:");		
		lblISBN = new JLabel(book.isbn);		
		lblSubjectLabel = new JLabel("Subject:");		
		lblSubject = new JLabel(book.subject);		
		lblCategoryLabel = new JLabel("Category:");		
		lblCategory = new JLabel(book.category);		
		lblAuthorLabel = new JLabel("Author:");		
		lblAuthor = new JLabel(book.author);		
		lblPublisherLabel = new JLabel("Publisher:");		
		lblPublisher = new JLabel(book.pubName);
		lblYearLabel = new JLabel("Year:");		
		lblYear = new JLabel(Integer.toString(book.year));
		
		lblPriceLabel = new JLabel("List price:");
		lblPriceLabel.setFont(new Font(lblPriceLabel.getFont().getFontName(), Font.BOLD, lblPriceLabel.getFont().getSize()));
		lblPriceLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblPrice = new JLabel(String.format("$%.2f", book.price));
		lblPrice.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblTotalLabel = new JLabel("Total:");
		lblTotalLabel.setFont(new Font(lblTotalLabel.getFont().getFontName(), Font.BOLD, lblTotalLabel.getFont().getSize()));
		lblTotalLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblTotal = new JLabel();
		lblTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblQuantityInStockLabel = new JLabel("Quantity in Stock:");
		lblQuantityInStockLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblQuantityInStockLabel.setFont(new Font(lblQuantityInStockLabel.getFont().getFontName(), Font.BOLD, lblQuantityInStockLabel.getFont().getSize()));
		
		btnPub = new JButton("?");
		if(isHistory)
			btnPub.setVisible(false);
		else
			btnPub.setVisible(true);
		btnPub.setFont(new Font(lblPriceLabel.getFont().getFontName(), Font.BOLD, 8));
		btnPub.setMinimumSize(new Dimension(14,14));
		btnPub.setPreferredSize(new Dimension(14,14));
		btnPub.setMaximumSize(new Dimension(14,14));
		btnPub.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
		btnPub.setHorizontalAlignment(SwingConstants.CENTER);
		btnPub.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, book.pubName + "\n" + "<HTML>" +  pubAddress + "<br><br>" + pubPhone + "</HTML>", "Publisher Contact Information",JOptionPane.PLAIN_MESSAGE);
			}
		});
		

		btnAuth = new JButton("?");
		btnAuth.setVisible(false);
		btnAuth.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, book.author + "\n" + "<HTML>" + authAddress + "</HTML>", "Author Contact Information",JOptionPane.PLAIN_MESSAGE);
			}
		});
		if(admin) {
			if(isHistory)
				btnAuth.setVisible(false);
			else
				btnAuth.setVisible(true);
			btnAuth.setFont(new Font(lblPriceLabel.getFont().getFontName(), Font.BOLD, 8));
			btnAuth.setMinimumSize(new Dimension(14,14));
			btnAuth.setPreferredSize(new Dimension(14,14));
			btnAuth.setMaximumSize(new Dimension(14,14));
			btnAuth.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
			btnAuth.setHorizontalAlignment(SwingConstants.CENTER);
		}
		
		dynamicButton = new JButton("Buy");
		if(this instanceof OrderedBookPanel) {
			dynamicButton.setVisible(false);
		} else if(admin) {
			dynamicButton = new JButton("Edit");
		}
		if(book.quantity == 0 && !admin) {
			dynamicButton.setEnabled(false);
		}
		dynamicButton.setPreferredSize(new Dimension(84,84));
		dynamicButton.setMinimumSize(new Dimension(84,84));
		dynamicButton.setMaximumSize(new Dimension(84,84));
		dynamicButton.setBackground(gray);
		dynamicButton.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(admin)
				{
					new EditInventoryDialog(null, (Product)book, con);
					updateQuantity();
				}
				else
				{
					CartItemPanel newItem = new CartItemPanel(false, book.productId, book.title, book.price, 1, book.quantity, cart);
					cart.addResult(newItem);
				}
			}
		});
		
		lblQuantityInStock = new JLabel(Integer.toString(book.quantity));
		lblQuantityInStock.setHorizontalAlignment(SwingConstants.RIGHT);
		
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		layout.setHorizontalGroup(
			layout.createSequentialGroup()
				.addGap(120)
				.addGroup(layout.createParallelGroup()
					.addComponent(lblTitle)
					.addGroup(layout.createSequentialGroup()
						.addGap(20)
						.addGroup(layout.createParallelGroup()
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblISBNLabel)
								.addComponent(lblISBN)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblPublisherLabel)
								.addComponent(lblPublisher)
								.addGap(10)
								.addComponent(btnPub)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblYearLabel)
								.addComponent(lblYear)
							)
						)
						.addGap(30)
						.addGroup(layout.createParallelGroup()
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblAuthorLabel)
								.addComponent(lblAuthor)
								.addGap(10)
								.addComponent(btnAuth)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblSubjectLabel)
								.addComponent(lblSubject)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblCategoryLabel)
								.addComponent(lblCategory)
							)
						)
						.addGap(10, 10, 1000)
						.addGroup(layout.createParallelGroup()
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblPriceLabel)
								.addComponent(lblPrice)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblQuantityInStockLabel)
								.addComponent(lblQuantityInStock)
							)
							.addGroup(layout.createSequentialGroup()
								.addComponent(lblTotalLabel)
								.addComponent(lblTotal)
							)
						)
					)
				)
				.addGap(10)
				.addComponent(dynamicButton)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addComponent(lblTitle)
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblISBNLabel)
								.addComponent(lblISBN)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblPublisherLabel)
								.addComponent(lblPublisher)
								.addComponent(btnPub)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblYearLabel)
								.addComponent(lblYear)
							)
						)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblAuthorLabel)
								.addComponent(lblAuthor)
								.addComponent(btnAuth)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblSubjectLabel)
								.addComponent(lblSubject)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblCategoryLabel)
								.addComponent(lblCategory)
							)
						)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblPriceLabel)
								.addComponent(lblPrice)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblQuantityInStockLabel)
								.addComponent(lblQuantityInStock)
							)
							.addGroup(layout.createParallelGroup()
									.addComponent(lblTotalLabel)
									.addComponent(lblTotal)
							)
						)
					)
				)
				.addComponent(dynamicButton)
		);
		layout.linkSize(SwingConstants.HORIZONTAL, lblISBNLabel, lblSubjectLabel, lblCategoryLabel, lblAuthorLabel, lblPublisherLabel,lblYearLabel);
		layout.linkSize(SwingConstants.HORIZONTAL, lblPrice, lblQuantityInStockLabel, lblQuantityInStock,lblPriceLabel, lblTotalLabel, lblTotal);
		setLayout(layout);
		
		ImageIcon background = new ImageIcon(getClass().getResource("book.png"));
		iconLabel = new JLabel();
		iconLabel.setBounds(5, 5, background.getIconWidth(), background.getIconHeight());
		iconLabel.setIcon(background);
		add(iconLabel);
		
		if(isHistory)
		{
			setBorder(new CompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white), new CompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black), BorderFactory.createEmptyBorder(0,0,4,0))));
		}
		else
		{
			setBorder(new CompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white), BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black)));
		}
		
		lblTotalLabel.setVisible(false);
		lblTotal.setVisible(false);
	}
	
	public void updateQuantity() {
		lblQuantityInStock.setText(Integer.toString(book.quantity));
		revalidate();
	}
}
