package comp4410.booksRUs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;

@SuppressWarnings("serial")
public class DVDPanel extends ResultPanel
{
	private DVD dvd;
	JLabel lblQuantityInStockLabel;
	JLabel lblPriceLabel;
	JLabel lblPrice;
	JLabel lblYearLabel;
	JLabel lblYear;
	JLabel lblTotalLabel;
	JLabel lblTotal;
	
	JLabel lblGenreLabel;
	JLabel lblGenre;
	
	JLabel lblStreetLabel;
	JLabel lblStreet;
	JLabel lblCityStateLabel;
	JLabel lblCityState;
	JLabel lblZipLabel;
	JLabel lblZip;
	
	
	Connection con;
	
	JLabel iconLabel;
	
	boolean isHistory = false;
	boolean admin;
	
	JButton dynamicButton;
	JButton sequelButton;
	CartPanel cart;
	MainFrame parent;
	JLabel lblQuantityInStock;
	
	public Color gray = new Color(60,60,60,255);
	String orderNum;

	public DVDPanel(MainFrame parent, CartPanel p, DVD d, boolean admin, Connection con) 
	{
		this.parent = parent;
		this.con = con;
		this.admin = admin;
		cart = p;
		dvd = d;
		try {
		setupPanel();
		} catch(SQLException e) {
			e.printStackTrace();  
		}
	}
	
	public DVDPanel(MainFrame parent, CartPanel p, DVD d, boolean admin, Connection con, boolean h, String orderNum) throws SQLException 
	{
		this.parent = parent;
		this.con = con;
		this.admin = admin;
		cart = p;
		dvd = d;
		this.isHistory = h;
		this.orderNum = orderNum;
		setupPanel();
	}
	
	private void setupPanel() throws SQLException
	{
		JLabel lblTitle = new JLabel(String.format("<HTML><U>%s</U></HTML>", dvd.title));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblDirectorLabel = new JLabel("Director:");		
		JLabel lblDirector = new JLabel(dvd.director);		
		lblGenreLabel = new JLabel("Genre:");		
		lblGenre = new JLabel(dvd.genre);		
		lblYearLabel = new JLabel("Year:");		
		lblYear = new JLabel(Integer.toString(dvd.year));
		
		if(isHistory) {
			
			String street, city, state, zip;		
			String query = String.format("Select DISTINCT * From OrderTransactions O NATURAL JOIN Customers WHERE O.ordernum = %s", orderNum);
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);		
			rs.next();
			
			street = rs.getString(rs.findColumn("address"));
			city = rs.getString(rs.findColumn("city"));
			state = rs.getString(rs.findColumn("state"));
			zip = rs.getString(rs.findColumn("zip"));		
		
			lblStreetLabel = new JLabel("Shipped To:");		
			lblStreet = new JLabel(street);
			
			lblCityStateLabel = new JLabel(" ");		
			lblCityState = new JLabel(city + ", " + state);
			
			lblZipLabel = new JLabel(" ");		
			lblZip = new JLabel(zip);
		
		}
		lblPriceLabel = new JLabel("List price:");
		lblPriceLabel.setFont(new Font(lblPriceLabel.getFont().getFontName(), Font.BOLD, lblPriceLabel.getFont().getSize()));
		lblPriceLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblPrice = new JLabel(String.format("$%.2f", dvd.price));
		lblPrice.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblTotalLabel = new JLabel("Total:");
		lblTotalLabel.setFont(new Font(lblTotalLabel.getFont().getFontName(), Font.BOLD, lblTotalLabel.getFont().getSize()));
		lblTotalLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblTotal = new JLabel();
		lblTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblQuantityInStockLabel = new JLabel("Quantity in Stock:");
		lblQuantityInStockLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblQuantityInStockLabel.setFont(new Font(lblQuantityInStockLabel.getFont().getFontName(), Font.BOLD, lblQuantityInStockLabel.getFont().getSize()));
		
		lblQuantityInStock = new JLabel(Integer.toString(dvd.quantity));
		lblQuantityInStock.setHorizontalAlignment(SwingConstants.RIGHT);

		dynamicButton = new JButton("Buy");
		if(this instanceof OrderedDVDPanel) {
			dynamicButton.setVisible(false);
		} else if(admin) {
			dynamicButton = new JButton("Edit");
		}
		if(dvd.quantity == 0 && !admin) {
			dynamicButton.setEnabled(false);
		}
		dynamicButton.setPreferredSize(new Dimension(84,84));
		dynamicButton.setMinimumSize(new Dimension(84,84));
		dynamicButton.setMaximumSize(new Dimension(84,84));
		dynamicButton.setBackground(gray);
		dynamicButton.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(admin)
				{
					new EditInventoryDialog(null, (Product)dvd, con);
					updateQuantity();
				}
				else
				{
					CartItemPanel newItem = new CartItemPanel(true, dvd.productId, dvd.title, dvd.price, 1, dvd.quantity, cart);
					cart.addResult(newItem);
				}
			}

			
		});
		
		// Clicking this will perform a DVD query that  will show this DVD's sequels in the results panel.
		
		sequelButton = new JButton();
		sequelButton.setPreferredSize(new Dimension(34,34));
		sequelButton.setMinimumSize(new Dimension(34,34));
		sequelButton.setMaximumSize(new Dimension(34,34));
		sequelButton.setBackground(gray);
		
		sequelButton.setToolTipText("This movie has sequels!");
		sequelButton.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.addResultsToTable(findSequels());
			}
		});
		
		String query;
		Statement st;
		ResultSet rs;
		
		try 
		{
			query = String.format("SELECT * From SequelOf WHERE sequelee = %d", dvd.productId);
			st = con.createStatement();
			rs = st.executeQuery(query);
			if(!rs.next()) {
				sequelButton.setVisible(false);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		
		
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		if(isHistory)
		{
			layout.setHorizontalGroup(
					layout.createSequentialGroup()
					.addGap(120)
					.addGroup(layout.createParallelGroup()
						.addComponent(lblTitle)
						.addGroup(layout.createSequentialGroup()
							.addGap(20)
							.addGroup(layout.createParallelGroup()
								.addGroup(layout.createSequentialGroup()
									.addComponent(lblDirectorLabel)
									.addComponent(lblDirector)
								)
								.addGroup(layout.createSequentialGroup()
									.addComponent(lblGenreLabel)
									.addComponent(lblGenre)
								)
								.addGroup(layout.createSequentialGroup()
									.addComponent(lblYearLabel)
									.addComponent(lblYear)
								)
							)
							.addGap(30)
							.addGroup(layout.createParallelGroup()
									.addGroup(layout.createSequentialGroup()
										.addComponent(lblStreetLabel)
										.addComponent(lblStreet)
									)
									.addGroup(layout.createSequentialGroup()
										.addComponent(lblCityStateLabel)
										.addComponent(lblCityState)
									)
									.addGroup(layout.createSequentialGroup()
										.addComponent(lblZipLabel)
										.addComponent(lblZip)
									)
								)
							.addGap(10, 10, 1000)
							.addGroup(layout.createParallelGroup()
								.addGroup(layout.createSequentialGroup()
									.addComponent(lblPriceLabel)
									.addComponent(lblPrice)
								)
								.addGroup(layout.createSequentialGroup()
									.addComponent(lblQuantityInStockLabel)
									.addComponent(lblQuantityInStock)
								)
								.addGroup(layout.createSequentialGroup()
									.addComponent(lblTotalLabel)
									.addComponent(lblTotal)
								)
							)
						)
					)
					.addGap(10)
					.addComponent(dynamicButton)
				);
				layout.setVerticalGroup(
					layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblTitle)
							.addGroup(layout.createParallelGroup()
								.addGroup(layout.createSequentialGroup()
									.addGroup(layout.createParallelGroup()
										.addComponent(lblDirectorLabel)
										.addComponent(lblDirector)
									)
									.addGroup(layout.createParallelGroup()
										.addComponent(lblGenreLabel)
										.addComponent(lblGenre)
									)
									.addGroup(layout.createParallelGroup()
										.addComponent(lblYearLabel)
										.addComponent(lblYear)
									)
								)
								
								.addGroup(layout.createSequentialGroup()
										.addGroup(layout.createParallelGroup()
											.addComponent(lblStreetLabel)
											.addComponent(lblStreet)
										)
										.addGroup(layout.createParallelGroup()
											.addComponent(lblCityStateLabel)
											.addComponent(lblCityState)
										)
										.addGroup(layout.createParallelGroup()
											.addComponent(lblZipLabel)
											.addComponent(lblZip)
										)
								)
								.addGroup(layout.createSequentialGroup()
									.addGroup(layout.createParallelGroup()
										.addComponent(lblPriceLabel)
										.addComponent(lblPrice)
									)
									.addGroup(layout.createParallelGroup()
										.addComponent(lblQuantityInStockLabel)
										.addComponent(lblQuantityInStock)
									)
									.addGroup(layout.createParallelGroup()
										.addComponent(lblTotalLabel)
										.addComponent(lblTotal)
									)
								)
							)
						)
						.addComponent(dynamicButton)
				);

				layout.linkSize(SwingConstants.HORIZONTAL, lblGenreLabel, lblDirectorLabel, lblYearLabel,lblStreetLabel,lblZipLabel,lblCityStateLabel);
				layout.linkSize(SwingConstants.HORIZONTAL, lblPrice, lblQuantityInStockLabel, lblQuantityInStock,lblPriceLabel,lblTotalLabel,lblTotal);
		}
		else
		{
		layout.setHorizontalGroup(
			layout.createSequentialGroup()
			.addGap(120)
			.addGroup(layout.createParallelGroup()
				.addComponent(lblTitle)
				.addGroup(layout.createSequentialGroup()
					.addGap(20)
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblDirectorLabel)
							.addComponent(lblDirector)
						)
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblGenreLabel)
							.addComponent(lblGenre)
						)
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblYearLabel)
							.addComponent(lblYear)
						)
					)
					.addGap(50)
					.addComponent(sequelButton)
					.addGap(10, 100, 1000)
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblPriceLabel)
							.addComponent(lblPrice)
						)
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblQuantityInStockLabel)
							.addComponent(lblQuantityInStock)
						)
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblTotalLabel)
							.addComponent(lblTotal)
						)
					)
				)
			)
			.addGap(10)
			.addComponent(dynamicButton)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addComponent(lblTitle)
					.addGroup(layout.createParallelGroup()
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblDirectorLabel)
								.addComponent(lblDirector)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblGenreLabel)
								.addComponent(lblGenre)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblYearLabel)
								.addComponent(lblYear)
							)
						)
						.addGroup(layout.createSequentialGroup()
								.addComponent(sequelButton)
						)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(lblPriceLabel)
								.addComponent(lblPrice)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblQuantityInStockLabel)
								.addComponent(lblQuantityInStock)
							)
							.addGroup(layout.createParallelGroup()
								.addComponent(lblTotalLabel)
								.addComponent(lblTotal)
							)
						)
					)
				)
				.addComponent(dynamicButton)
		);

		layout.linkSize(SwingConstants.HORIZONTAL, lblGenreLabel, lblDirectorLabel, lblYearLabel);
		layout.linkSize(SwingConstants.HORIZONTAL, lblPrice, lblQuantityInStockLabel, lblQuantityInStock,lblPriceLabel,lblTotalLabel,lblTotal);
		
		}
		
		setLayout(layout);
		
		ImageIcon background = new ImageIcon(getClass().getResource("film.png"));
		iconLabel = new JLabel();
		iconLabel.setBounds(5, 5, background.getIconWidth(), background.getIconHeight());
		iconLabel.setIcon(background);
		add(iconLabel);
		
		sequelButton.setIcon(new ImageIcon(getScaledImage(background.getImage(), 24, 24)));
		sequelButton.setHorizontalAlignment(SwingConstants.CENTER);
		
		if(isHistory)
		{
			setBorder(new CompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white), new CompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black), BorderFactory.createEmptyBorder(0,0,4,0))));
		}
		else
		{
			setBorder(new CompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.white), BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black)));
		}
		
		lblTotalLabel.setVisible(false);
		lblTotal.setVisible(false);
	}
	
	public void updateQuantity() {
		lblQuantityInStock.setText(Integer.toString(dvd.quantity));
		revalidate();
	}
	
	private Image getScaledImage(Image srcImg, int w, int h){
	    BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2 = resizedImg.createGraphics();

	    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(srcImg, 0, 0, w, h, null);
	    g2.dispose();

	    return resizedImg;
	}
	
	private ArrayList<ResultPanel> findSequels() 
	{
		ArrayList<ResultPanel> results = new ArrayList<ResultPanel>();

		String query;
		Statement st;
		ResultSet rs;
		
		try 
		{
			query = "Select DISTINCT * From DVDs D NATURAL JOIN Sequelof S NATURAL JOIN PRODUCTS WHERE D.productId = S.sequel AND S.sequelee = "+ dvd.productId;
			st = con.createStatement();
			rs = st.executeQuery(query);
			while(rs.next()) 
			{
				DVD d = new DVD(
					rs.getInt(rs.findColumn("productId")),
					rs.getString(rs.findColumn("director")),
					rs.getString(rs.findColumn("genre")),
					rs.getDouble(rs.findColumn("price")),
					rs.getInt(rs.findColumn("quantity")),
					rs.getString(rs.findColumn("title")),
					rs.getInt(rs.findColumn("year")));
				results.add(new DVDPanel(parent, cart, d, admin, con));	
			}
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return results;
	}
	
}
