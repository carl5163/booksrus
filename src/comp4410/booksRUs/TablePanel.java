package comp4410.booksRUs;

import java.awt.*;
import java.util.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class TablePanel extends JPanel
{
	public static final int PANEL_SIZE = 110;
	
	private JPanel scrollingPanel;
	
	public TablePanel() 
	{
		setLayout(new BorderLayout());

		scrollingPanel = new JPanel();
		scrollingPanel.setBackground(Color.WHITE);
		scrollingPanel.setLayout(new BoxLayout(scrollingPanel, BoxLayout.Y_AXIS));
	
		JScrollPane scroll = new JScrollPane(scrollingPanel);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.getVerticalScrollBar().setUnitIncrement(12);
		
		add(scroll, BorderLayout.CENTER);
	}

	public void addResults(ArrayList<ResultPanel> results) 
	{
		scrollingPanel.removeAll();
		scrollingPanel.setLayout(new BoxLayout(scrollingPanel, BoxLayout.Y_AXIS));
		int numResults = results.size();
		
		if(numResults == 0) {
			JLabel label = new JLabel("<HTML><U>No Results</U></HTML");
			label.setHorizontalAlignment(SwingConstants.CENTER);
			scrollingPanel.setLayout(new GridLayout(numResults,1,1,0));
			scrollingPanel.add(label);
		}

		for(ResultPanel p : results) 
		{
			scrollingPanel.add(p);
		}

		updateSize();
	}
	
	public void updateSize()
	{
		scrollingPanel.revalidate();
		scrollingPanel.repaint();
	}
}
