package comp4410.booksRUs;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.text.ParseException;
import java.util.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class SearchPanel extends JPanel 
{
	private JTextField textField;
	private JButton btnSearch;

	private JRadioButton rdbtnAuthor;		
	private JRadioButton rdbtnActor;		
	private JRadioButton rdbtnPublisher;		
	private JRadioButton rdbtnDirector;		
	private JRadioButton rdbtnSubject;		
	private JRadioButton rdbtnGenre;
	private JRadioButton rdbtnCustomerName;		
	private JRadioButton rdbtnCustomerEmail;
	
	private JRadioButton rdbtnCustomerOrder;
	
	private MainFrame parent;
	
	private JCheckBox checkSearchType;
	private Connection con;
	private boolean admin;
	private CartPanel cart;
	
	public Color gray = new Color(60,60,60,255);
	
	public SearchPanel(CartPanel p, MainFrame parent, Connection connection, boolean admin) 
	{
		cart = p;
		this.parent = parent;
		this.setBackground(gray);
		con = connection;	
		this.admin = admin;
		
		JLabel lblSearchTerm = new JLabel("Search:");
		lblSearchTerm.setForeground(Color.white);
		
		textField = new JTextField();
		textField.setPreferredSize(new Dimension(180,25));
		
		checkSearchType = new JCheckBox("Advanced options");
		checkSearchType.setBackground(gray);
		checkSearchType.setForeground(Color.white);
		
		JLabel lblSearchAttribute = new JLabel("Search Attribute:");
		lblSearchAttribute.setForeground(Color.white);
				
		rdbtnAuthor = new JRadioButton("Author");	
		rdbtnAuthor.setBackground(gray);
		rdbtnAuthor.setForeground(Color.white);
		
		rdbtnActor = new JRadioButton("Actor");
		rdbtnActor.setBackground(gray);
		rdbtnActor.setForeground(Color.white);
		
		rdbtnPublisher = new JRadioButton("Publisher");
		rdbtnPublisher.setBackground(gray);
		rdbtnPublisher.setForeground(Color.white);
		
		rdbtnDirector = new JRadioButton("Director");
		rdbtnDirector.setBackground(gray);
		rdbtnDirector.setForeground(Color.white);
		
		rdbtnSubject = new JRadioButton("Subject");
		rdbtnSubject.setBackground(gray);
		rdbtnSubject.setForeground(Color.white);
		
		rdbtnGenre = new JRadioButton("Genre");
		rdbtnGenre.setBackground(gray);
		rdbtnGenre.setForeground(Color.white);
		
		rdbtnCustomerName = new JRadioButton("Customer Name");
		rdbtnCustomerName.setBackground(gray);
		rdbtnCustomerName.setForeground(Color.white);
		
		rdbtnCustomerEmail = new JRadioButton("Customer Email");
		rdbtnCustomerEmail.setBackground(gray);
		rdbtnCustomerEmail.setForeground(Color.white);
		
		rdbtnCustomerOrder = new JRadioButton("Customer Order");
		rdbtnCustomerOrder.setBackground(gray);
		rdbtnCustomerOrder.setForeground(Color.white);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnAuthor);
		group.add(rdbtnActor);
		group.add(rdbtnPublisher);
		group.add(rdbtnDirector);
		group.add(rdbtnSubject);
		group.add(rdbtnGenre);
		group.add(rdbtnCustomerName);
		group.add(rdbtnCustomerEmail);
		group.add(rdbtnCustomerOrder);
		
		lblSearchAttribute.setVisible(false);
		rdbtnAuthor.setVisible(false);
		rdbtnAuthor.setSelected(true);
		rdbtnActor.setVisible(false);
		rdbtnPublisher.setVisible(false);
		rdbtnDirector.setVisible(false);
		rdbtnSubject.setVisible(false);
		rdbtnGenre.setVisible(false);
		rdbtnCustomerName.setVisible(false);
		rdbtnCustomerEmail.setVisible(false);
		rdbtnCustomerOrder.setVisible(false);
		
		btnSearch = new JButton("Search");
		btnSearch.setBackground(gray);
		btnSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().trim().length() > 0) {
					try {
						doSearch();
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
				}
			}
			
		});
		
		JButton btnProfile = new JButton("My Profile");
		btnProfile.setBackground(gray);
		btnProfile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
							
				try {
					String query = String.format("Select DISTINCT * From Customers WHERE email = '%s'", parent.userEmail);
					Statement st = con.createStatement();
					ResultSet rs = st.executeQuery(query);
					rs.next();
					Customer c = new Customer(rs.getString(rs.findColumn("name")), rs.getString(rs.findColumn("email")), rs.getString(rs.findColumn("phone")), rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
					new EditCustomerDialog(c, con, parent, true);  //Pass in user/customer.
				} catch (SQLException e1) {
					e1.printStackTrace();
				}		
			}
			
		});
		if(admin) {
			btnProfile.setVisible(false);
		}
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.setBackground(gray);
		btnLogout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parent.logOut();
			}
			
		});
		
		checkSearchType.addActionListener(
       		new ActionListener()
       		{
       			public void actionPerformed(ActionEvent e)
       			{
       				if(checkSearchType.isSelected())
       				{
       					lblSearchAttribute.setVisible(true);
       					rdbtnAuthor.setVisible(true);
       					rdbtnActor.setVisible(true);
       					rdbtnPublisher.setVisible(true);
       					rdbtnDirector.setVisible(true);
       					rdbtnSubject.setVisible(true);
       					rdbtnGenre.setVisible(true);
       					if(admin)
       					{
       						rdbtnCustomerName.setVisible(true);
       						rdbtnCustomerEmail.setVisible(true);
       						rdbtnCustomerOrder.setVisible(true);
       					}
       					else
       					{
       						rdbtnCustomerName.setVisible(false);
       						rdbtnCustomerEmail.setVisible(false);
       						rdbtnCustomerOrder.setVisible(false);
       					}
       				}
       				else
       				{
       					lblSearchAttribute.setVisible(false);
       					rdbtnAuthor.setVisible(false);
       					rdbtnActor.setVisible(false);
       					rdbtnPublisher.setVisible(false);
       					rdbtnDirector.setVisible(false);
       					rdbtnSubject.setVisible(false);
       					rdbtnGenre.setVisible(false);
       					rdbtnCustomerName.setVisible(false);
   						rdbtnCustomerEmail.setVisible(false);
   						rdbtnCustomerOrder.setVisible(false);
       				}
       			}
       		}
        );
		
		parent.getRootPane().setDefaultButton(btnSearch);
		
		JLabel lblOther = new JLabel("Other:");
		lblOther.setForeground(Color.white);	
		
		JButton btnHistory = new JButton("My Purchase History");
		btnHistory.setPreferredSize(new Dimension(180,25));
		btnHistory.setBackground(gray);
		btnHistory.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				ArrayList<ResultPanel> results = new ArrayList<ResultPanel>();
				String query;
				Statement st;
				ResultSet rs;
				try
				{
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN Partof NATURAL JOIN Books NATURAL JOIN Products NATURAL JOIN Authors NATURAL JOIN Publishers WHERE EMAIL = '%s'", parent.userEmail);
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						Book b = new Book(
							rs.getInt(rs.findColumn("productId")),
							rs.getString(rs.findColumn("ISBN")),
							rs.getString(rs.findColumn("subject")),
							rs.getString(rs.findColumn("category")),
							rs.getString(rs.findColumn("pubName")),
							rs.getDouble(rs.findColumn("price")),
							rs.getInt(rs.findColumn("quantity")),
							String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
							rs.getInt(rs.findColumn("year")),
							rs.getString(rs.findColumn("name")));	
						String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
						String phone = rs.getString(rs.findColumn("phone"));
						String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
						results.add(new OrderedBookPanel(cart, b, rs.getString(rs.findColumn("date")), pubAddress, phone, authAddress, admin, con, rs.getString(rs.findColumn("ordernum"))));	
					}
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN Partof NATURAL JOIN DvDs NATURAL JOIN Products WHERE EMAIL = '%s'", parent.userEmail);
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						DVD d = new DVD(
							rs.getInt(rs.findColumn("productId")),
							rs.getString(rs.findColumn("director")),
							rs.getString(rs.findColumn("genre")),
							rs.getDouble(rs.findColumn("price")),
							rs.getInt(rs.findColumn("quantity")),
							String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
							rs.getInt(rs.findColumn("year")));
						results.add(new OrderedDVDPanel(parent, cart, d, rs.getString(rs.findColumn("date")), admin, con, rs.getString(rs.findColumn("orderNum"))));	
					}
				}
				catch(SQLException ex)
				{
					ex.printStackTrace();
				}
				parent.addResultsToTable(results);
			}
		});
		
		lblOther.setVisible(!admin);
		btnHistory.setVisible(!admin);
		
		GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        hGroup.addGap(10);
		hGroup.addGroup(layout.createParallelGroup()
			.addComponent(lblSearchTerm)
			.addComponent(textField)
			.addComponent(checkSearchType)
			.addComponent(lblSearchAttribute)
			.addComponent(rdbtnCustomerName)
			.addComponent(rdbtnCustomerEmail)
			.addComponent(rdbtnCustomerOrder)
			.addComponent(rdbtnAuthor)
			.addComponent(rdbtnActor)
			.addComponent(rdbtnPublisher)
			.addComponent(rdbtnDirector)
			.addComponent(rdbtnSubject)
			.addComponent(rdbtnGenre)
			.addComponent(btnSearch)
			.addComponent(lblOther)
			.addComponent(btnProfile)
			.addComponent(btnHistory)
			.addComponent(btnLogout)
		);
		
		hGroup.addGap(10);
		layout.setHorizontalGroup(hGroup);
		
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		vGroup.addGroup(layout.createSequentialGroup()
			.addComponent(lblSearchTerm)
			.addComponent(textField)
			.addComponent(checkSearchType)
			
			.addGap(10)
			.addComponent(lblSearchAttribute)
			.addComponent(rdbtnCustomerName)
			.addComponent(rdbtnCustomerEmail)
			.addComponent(rdbtnCustomerOrder)
			.addGap(10)
			.addComponent(rdbtnAuthor)
			.addComponent(rdbtnActor)
			.addComponent(rdbtnPublisher)
			.addComponent(rdbtnDirector)
			.addComponent(rdbtnSubject)
			.addComponent(rdbtnGenre)
			.addComponent(btnSearch)
			.addGap(30)
			.addComponent(lblOther)
			.addComponent(btnProfile)
			.addComponent(btnHistory)
			.addGap(10, 10, 1000)
			.addComponent(btnLogout)
		);
      
        layout.linkSize(SwingConstants.HORIZONTAL, textField, btnSearch, btnHistory,btnLogout,btnProfile);
        layout.linkSize(SwingConstants.VERTICAL, textField, btnSearch, btnHistory,btnLogout,btnProfile);

		layout.setVerticalGroup(vGroup);
	}
	
	private int isTopTen(int[] topTen, int pid)
	{
		int ret = 0;
		int i = 0;
		for(i = 0; i < 10; i++) 
		{
			if(pid == topTen[i])
			{
				ret = i;
				break;
			}
		}
		if(i==10) {
			return ret;
		}
		return ret+1;
	}
	
	private void doSearch() throws ParseException 
	{
		String query;
		Statement st;
		ResultSet rs;
		ResultSetMetaData rsmd;
		ArrayList<ResultPanel> results;		
		String[] keywords = textField.getText().trim().split("\\s+");
		String[] tables = {"Books","DVDs"};
		
		results = new ArrayList<ResultPanel>();
		if(checkSearchType.isSelected()) 
		{
			try 
			{
				int[] topTen = AdminPanel.getTopTenArray(con);
				
				//Advanced Search
				if(rdbtnAuthor.isSelected()) 
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM Books B NATURAL JOIN Authors A NATURAL JOIN Products NATURAL JOIN Publishers WHERE A.name LIKE '%%%s%%'", kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String title = rs.getString(rs.findColumn("title"));
							int rank = isTopTen(topTen, rs.getInt(rs.findColumn("productId")));
							
							Book b = new Book(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("ISBN")),
								rs.getString(rs.findColumn("subject")),
								rs.getString(rs.findColumn("category")),
								rs.getString(rs.findColumn("pubName")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantity")),
								title,
								rs.getInt(rs.findColumn("year")),
								rs.getString(rs.findColumn("name")));
							if(rank > 0) 								
							{
								String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
								String phone = rs.getString(rs.findColumn("phone"));
								String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
								results.add(new TopTenBookPanel(cart, b, rank, pubAddress, phone, authAddress, admin, con));
							} else 
							{
								String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
								String phone = rs.getString(rs.findColumn("phone"));
								String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
								results.add(new BookPanel(cart, b, pubAddress, phone, authAddress, admin, con));								
							}
						}
					}
				}
				else if(rdbtnActor.isSelected())
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM DVDs D NATURAL JOIN Actors A NATURAL JOIN PlayedIn P NATURAL JOIN Products WHERE A.actorId = P.actorId AND P.productId = D.productId AND A.name LIKE '%%%s%%'", kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String title = rs.getString(rs.findColumn("title"));
							int rank = isTopTen(topTen, rs.getInt(rs.findColumn("productId")));
							DVD d = new DVD(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("director")),
								rs.getString(rs.findColumn("genre")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantity")),
								title,
								rs.getInt(rs.findColumn("year")));
							if(rank > 0) 								
							{
								results.add(new TopTenDVDPanel(parent, cart, d, rank, admin, con));
							} else 
							{
								results.add(new DVDPanel(parent, cart, d, admin, con));								
							}
						}
					}
				}
				else if(rdbtnPublisher.isSelected())
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM Books B NATURAL JOIN Products NATURAL JOIN Authors NATURAL JOIN Publishers WHERE B.pubName LIKE '%%%s%%'", kw, kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String title = rs.getString(rs.findColumn("title"));
							int rank = isTopTen(topTen, rs.getInt(rs.findColumn("productId")));
							Book b = new Book(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("ISBN")),
								rs.getString(rs.findColumn("subject")),
								rs.getString(rs.findColumn("category")),
								rs.getString(rs.findColumn("pubName")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantity")),
								title,
								rs.getInt(rs.findColumn("year")),
								rs.getString(rs.findColumn("name")));
							if(rank > 0) 								
							{
								String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
								String phone = rs.getString(rs.findColumn("phone"));
								String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
								results.add(new TopTenBookPanel(cart, b, rank, pubAddress, phone, authAddress, admin, con));
							} 
							else 
							{
								String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
								String phone = rs.getString(rs.findColumn("phone"));
								String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
								results.add(new BookPanel(cart, b, pubAddress, phone, authAddress, admin, con));									
							}
						}
					}
				}
				else if(rdbtnDirector.isSelected())
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM DVDs D NATURAL JOIN Products WHERE D.director LIKE '%%%s%%'", kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String title = rs.getString(rs.findColumn("title"));
							int rank = isTopTen(topTen, rs.getInt(rs.findColumn("productId")));
							DVD d = new DVD(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("director")),
								rs.getString(rs.findColumn("genre")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantity")),
								title,
								rs.getInt(rs.findColumn("year")));
							if(rank > 0) 								
							{

								results.add(new TopTenDVDPanel(parent, cart, d, rank, admin, con));
							} else 
							{
								results.add(new DVDPanel(parent, cart, d, admin, con));								
							}
						}
					}
				}
				else if(rdbtnSubject.isSelected())
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM Books B NATURAL JOIN Products NATURAL JOIN Authors NATURAL JOIN Publishers WHERE B.subject LIKE '%%%s%%'", kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String title = rs.getString(rs.findColumn("title"));
							int rank = isTopTen(topTen, rs.getInt(rs.findColumn("productId")));
							Book b = new Book(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("ISBN")),
								rs.getString(rs.findColumn("subject")),
								rs.getString(rs.findColumn("category")),
								rs.getString(rs.findColumn("pubName")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantity")),
								title,
								rs.getInt(rs.findColumn("year")),
								rs.getString(rs.findColumn("name")));
							if(rank > 0) 								
							{
								String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
								String phone = rs.getString(rs.findColumn("phone"));
								String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
								results.add(new TopTenBookPanel(cart, b, rank, pubAddress, phone, authAddress, admin, con));
							} 
							else 
							{
								String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
								String phone = rs.getString(rs.findColumn("phone"));
								String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
								results.add(new BookPanel(cart, b, pubAddress, phone, authAddress, admin, con));								
							}
						}
					}
				}
				else if(rdbtnGenre.isSelected())
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM DVDs D NATURAL JOIN Products WHERE D.genre LIKE '%%%s%%'", kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String title = rs.getString(rs.findColumn("title"));
							int rank = isTopTen(topTen, rs.getInt(rs.findColumn("productId")));
							DVD d = new DVD(
								rs.getInt(rs.findColumn("productId")),
								rs.getString(rs.findColumn("director")),
								rs.getString(rs.findColumn("genre")),
								rs.getDouble(rs.findColumn("price")),
								rs.getInt(rs.findColumn("quantity")),
								title,
								rs.getInt(rs.findColumn("year")));
							if(rank > 0) 								
							{
								results.add(new TopTenDVDPanel(parent, cart, d, rank, admin, con));
							} else 
							{
								results.add(new DVDPanel(parent, cart, d, admin, con));								
							}	
						}
					}
				}
				else if(rdbtnCustomerName.isSelected())
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM Customers C WHERE C.name LIKE '%%%s%%'", kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String phone = rs.getString(rs.findColumn("phone"));
							results.add(new CustomerPanel(parent, new Customer(rs.getString(rs.findColumn("name")), rs.getString(rs.findColumn("email")), phone, rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip"))), con));
						}
					}
				}
				else if(rdbtnCustomerEmail.isSelected())
				{
					for(String kw : keywords) {
						query = String.format("SELECT DISTINCT * FROM Customers C WHERE C.email LIKE '%%%s%%'", kw);
						st = con.createStatement();
						rs = st.executeQuery(query);
						while(rs.next()) 
						{
							String phone = rs.getString(rs.findColumn("phone"));
							results.add(new CustomerPanel(parent, new Customer(rs.getString(rs.findColumn("name")), rs.getString(rs.findColumn("email")), phone, rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip"))), con));
						}
					}
				}
				else if(rdbtnCustomerOrder.isSelected())
				{
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN Partof NATURAL JOIN Books NATURAL JOIN Products NATURAL JOIN Authors NATURAL JOIN Publishers WHERE ordernum = '%s'", textField.getText().trim());
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						Book b = new Book(
							rs.getInt(rs.findColumn("productId")),
							rs.getString(rs.findColumn("ISBN")),
							rs.getString(rs.findColumn("subject")),
							rs.getString(rs.findColumn("category")),
							rs.getString(rs.findColumn("pubName")),
							rs.getDouble(rs.findColumn("price")),
							rs.getInt(rs.findColumn("quantityOrdered")),
							String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
							rs.getInt(rs.findColumn("year")),
							rs.getString(rs.findColumn("name")));	
						String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
						String phone = rs.getString(rs.findColumn("phone"));
						String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
						results.add(new OrderedBookPanel(null, b, rs.getString(rs.findColumn("date")), pubAddress, phone, authAddress, true, con, rs.getString(rs.findColumn("ordernum"))));	
					}
					query = String.format("Select DISTINCT * From OrderTransactions NATURAL JOIN Partof NATURAL JOIN DvDs NATURAL JOIN Products WHERE ordernum = '%s'", textField.getText().trim());
					st = con.createStatement();
					rs = st.executeQuery(query);
					while(rs.next()) 
					{
						DVD d = new DVD(
							rs.getInt(rs.findColumn("productId")),
							rs.getString(rs.findColumn("director")),
							rs.getString(rs.findColumn("genre")),
							rs.getDouble(rs.findColumn("price")),
							rs.getInt(rs.findColumn("quantityOrdered")),
							String.format("<HTML><U>%s</U></HTML>", rs.getString(rs.findColumn("title"))),
							rs.getInt(rs.findColumn("year")));
						results.add(new OrderedDVDPanel(parent, null, d, rs.getString(rs.findColumn("date")), true, con, rs.getString(rs.findColumn("orderNum"))));	
					}
					parent.addResultsToTable(results);
				}
				
			}
			catch(SQLException e) 
			{
				e.printStackTrace();
			}
		}
		else 
		{
			//Keyword Search
			try 
			{
				int[] topTen = AdminPanel.getTopTenArray(con);
				ArrayList<Integer> pids = new ArrayList<Integer>();
				for(String table : tables) 
				{
					query = String.format("Select * From %s NATURAL JOIN Products P", table);
					if(table.equalsIgnoreCase("BOOKS"))
					{
						query = query + " NATURAL JOIN AUTHORS NATURAL JOIN Publishers";
					}
					st = con.createStatement();
					rs = st.executeQuery(query);
					rsmd = rs.getMetaData();
									
					while(rs.next()) 
					{
						String title = rs.getString(rs.findColumn("title"));
						int rank = isTopTen(topTen, rs.getInt(rs.findColumn("productId")));
						for(int i = 1; i <= rsmd.getColumnCount(); i++) 
						{
							for(String kw : keywords)
							{
								String col = rs.getString(i).toUpperCase(); 
								if(col.contains(kw.toUpperCase())) 
								{
									if(table.equalsIgnoreCase("BOOKS"))
									{
										Book b = new Book(
											rs.getInt(rs.findColumn("productId")),
											rs.getString(rs.findColumn("ISBN")),
											rs.getString(rs.findColumn("subject")),
											rs.getString(rs.findColumn("category")),
											rs.getString(rs.findColumn("pubName")),
											rs.getDouble(rs.findColumn("price")),
											rs.getInt(rs.findColumn("quantity")),
											title,
											rs.getInt(rs.findColumn("year")),
											rs.getString(rs.findColumn("name")));
										if(!intContains(pids, rs.getInt(rs.findColumn("productId")))) {
											if(rank > 0) 								
											{
												String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
												String phone = rs.getString(rs.findColumn("phone"));
												String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
												results.add(new TopTenBookPanel(cart, b, rank, pubAddress, phone, authAddress, admin, con));
											} 
											else 
											{
												String pubAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("address")),rs.getString(rs.findColumn("city")), rs.getString(rs.findColumn("state")), rs.getString(rs.findColumn("zip")));
												String phone = rs.getString(rs.findColumn("phone"));
												String authAddress = String.format("%s<br>%s, %s, %s", rs.getString(rs.findColumn("authaddress")),rs.getString(rs.findColumn("authcity")), rs.getString(rs.findColumn("authstate")), rs.getString(rs.findColumn("authzip")));							
												results.add(new BookPanel(cart, b, pubAddress, phone, authAddress, admin, con));								
											}
											pids.add(rs.getInt(rs.findColumn("productId")));
										}
									}
									else
									{
										DVD d = new DVD(
											rs.getInt(rs.findColumn("productId")),
											rs.getString(rs.findColumn("director")),
											rs.getString(rs.findColumn("genre")),
											rs.getDouble(rs.findColumn("price")),
											rs.getInt(rs.findColumn("quantity")),
											title,
											rs.getInt(rs.findColumn("year")));
										if(!intContains(pids, rs.getInt(rs.findColumn("productId")))) {
											if(rank > 0) 								
											{
												results.add(new TopTenDVDPanel(parent, cart, d, rank, admin, con));
											} 
											else 
											{
												results.add(new DVDPanel(parent, cart, d, admin, con));								
											}	
											pids.add(rs.getInt(rs.findColumn("productId")));
										}									
									}
								}
							}						
						}					
					}
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
		parent.addResultsToTable(results);
	}
	
	public static boolean intContains(ArrayList<Integer> pids, int test) {
		for(int i : pids) {
			if(i == test) {
				return true;
			}
		}
		return false;
	}

	
}
