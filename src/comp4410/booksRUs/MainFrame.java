package comp4410.booksRUs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements ComponentListener
{
	public static final int CUSTOMER = 1;
	public static final int ADMIN = 2;
	
	public static boolean LOGGED_IN;
	
	private int userType;
	private String userString;
	private Connection connection;
	private TablePanel tablePanel;
	private JPanel statusBar;
	private JLabel statusLabel;
	private CartPanel cartPanel;
	
	String userEmail;
	
	public Color gray = new Color(60,60,60,255);
	
	public MainFrame()
	{
		connectToDatabase();
		logIn();
	}
	
	private void connectToDatabase()
	{
		try 
		{
	        Class.forName("com.mysql.jdbc.Driver");
	        connection = DriverManager.getConnection("jdbc:mysql://98.157.75.163/booksrus?autoReconnect=true&useSSL=false","root","thekid");
	        //connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1/booksrus?autoReconnect=true&useSSL=false","root","thekid");
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			exit(ERROR);
	    }
	}

	private void setupGUI()
	{
		Toolkit tk;
		tk = Toolkit.getDefaultToolkit();
		setTitle("Books R' Us");
		setIconImage(new ImageIcon(getClass().getResource("main.png")).getImage());
		setSize(new Dimension((int) (tk.getScreenSize().width*.75), (int) (tk.getScreenSize().height*.75)));
		setLocationRelativeTo(null);
		setResizable(true);
		setMinimumSize(new Dimension(1225,350));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		if(userType == CUSTOMER)
		{
			cartPanel = new CartPanel(connection, this);
			add(cartPanel, BorderLayout.EAST);
		}
		else
		{
			add(new AdminPanel(this, connection), BorderLayout.EAST);
		}
		
		add(new SearchPanel(cartPanel, this, connection, userType==ADMIN), BorderLayout.WEST);		
		tablePanel = new TablePanel();		
		add(tablePanel, BorderLayout.CENTER);
		
		statusLabel = new JLabel("Connected to database.");
		statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		statusBar = new JPanel();
		statusBar.setBackground(Color.white);
		statusBar.setLayout(new BoxLayout(statusBar, BoxLayout.X_AXIS));
		statusBar.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, gray));
		statusBar.add(new JLabel("  "));
		statusBar.add(statusLabel);
		add(statusBar, BorderLayout.SOUTH);
		
		addComponentListener(this);
		
	}
	
	public void setLoginSuccess(String type, String name, String email)
	{
		LOGGED_IN = true;
		userEmail = email;		
		
		if(type.equals("Admins")) {
			userType = ADMIN;
			userString = "Admin";
		}
		else 
		{
			userType = CUSTOMER;
			userString = "Customer";
		}
		setupGUI();
		setVisible(true);
		revalidate();
		setTitle(String.format("Books R' Us   |   %s: %s", userString, name));
	}
	
	public void logIn()
	{
		
		LOGGED_IN = false;
		
		userString = null;
		userType = 0;
		LoginDialog loginDlg = new LoginDialog(this, connection);
		loginDlg.setVisible(true);
		
	}
	
	public void logOut()
	{
		restart();
	}
	
	public static void exit(int condition)
	{
		System.exit(condition);
	}
	
	public static void restart()
	{
		BooksRUs.restart();
	}

	public void addResultsToTable(ArrayList<ResultPanel> arrayList) 
	{
		tablePanel.addResults(arrayList);
		statusLabel.setText(String.format("%d results found.", arrayList.size()));
		revalidate();
	}

	@Override
	public void componentResized(ComponentEvent e)
	{
		tablePanel.updateSize();
		tablePanel.revalidate();
		tablePanel.repaint();
	}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentShown(ComponentEvent e) {}

	public String getEmail() {
		return userEmail;
	}

	public String getDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		return formatter.format(System.currentTimeMillis());
	}
}
