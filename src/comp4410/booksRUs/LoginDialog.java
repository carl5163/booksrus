package comp4410.booksRUs;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class LoginDialog extends JFrame
{
	private JTextField tfEmail;
    private JPasswordField pfPassword;
    private JLabel lbEmail;
    private JLabel lbPassword;
    private JButton btnLogin;
    private JButton btnCancel;
    private JCheckBox checkAdmin;
    private Connection connection;
    
    public LoginDialog(MainFrame parent, Connection connection)
    {
    	this.connection = connection;
    	this.setLocationRelativeTo(parent);

    	setupGUI();
    	
    	lbEmail = new JLabel("Email: ");
    	lbPassword = new JLabel("Password: ");
    	tfEmail = new JTextField();
    	tfEmail.setPreferredSize(new Dimension(180,25));
    	
    	// CHANGE ME!!!
    	tfEmail.setText("silence.is.pointless@gmail.com");
    	
    	pfPassword = new JPasswordField();
    	pfPassword.setPreferredSize(new Dimension(180,25));
    	
    	// CHANGE ME!!!
    	pfPassword.setText("thekid");

    	checkAdmin = new JCheckBox("Login as Admin.");
    	btnLogin = new JButton("Login");
    	getRootPane().setDefaultButton(btnLogin);
    	btnCancel = new JButton("Cancel");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
		
        hGroup.addGap(20);
		hGroup.addGroup(layout.createParallelGroup()
			.addComponent(lbEmail)
			.addComponent(tfEmail)
			
			.addComponent(lbPassword)
			.addComponent(pfPassword)
			.addGroup(layout.createSequentialGroup()
					.addGap(30)
					.addComponent(checkAdmin)
			)
			.addComponent(btnLogin)
			.addComponent(btnCancel)
		);

		layout.setHorizontalGroup(hGroup);
		
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		
		vGroup.addGroup(layout.createSequentialGroup()
			.addComponent(lbEmail)
			.addComponent(tfEmail)
			
			.addGap(10)
			.addComponent(lbPassword)
			.addComponent(pfPassword)
			.addComponent(checkAdmin)
			.addGap(20)
		);			
		
		vGroup.addGroup(layout.createSequentialGroup()
			.addComponent(btnLogin)
			.addGap(10)
			.addComponent(btnCancel)
			.addGap(10)
		);
      
        layout.linkSize(SwingConstants.HORIZONTAL, btnLogin, btnCancel,tfEmail,pfPassword);
        layout.linkSize(SwingConstants.VERTICAL, btnLogin, btnCancel,tfEmail,pfPassword);

		layout.setVerticalGroup(vGroup);

		pack();
        setLocationRelativeTo(parent);
        setResizable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        addWindowListener(new WindowListener()
		{
            public void windowClosed(WindowEvent arg0) {}
            public void windowActivated(WindowEvent arg0) {}
            
            public void windowClosing(WindowEvent arg0)
            {
        		MainFrame.exit(0);
            }
            
            public void windowDeactivated(WindowEvent arg0) {}
            public void windowDeiconified(WindowEvent arg0) {}
            public void windowIconified(WindowEvent arg0) {}
            public void windowOpened(WindowEvent arg0) {}
        });
        
        btnLogin.addActionListener(
       		new ActionListener()
       		{
       			public void actionPerformed(ActionEvent e)
       			{
       				String[] details = new String[3];
       				details[0] = tfEmail.getText().trim();
       				details[1] = new String(pfPassword.getPassword());
       				details[2] = checkAdmin.isSelected() ? "Admins" : "Customers";
       				try
       				{
       					String name = login(details);
       					if(name == null)
       					{
       						pfPassword.setText("");
       						JOptionPane.showMessageDialog(null, "Failed to login: Email and password combination not found.", "Error", JOptionPane.ERROR_MESSAGE);
       					}
       					else
       					{
       						parent.setLoginSuccess(details[2], name, details[0]);
       						dispose();
       					}
       				}
       				catch (SQLException e1)
       				{
       					JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
       				}
       			}
       			
       		}
        );
        
        btnCancel.addActionListener(
        	new ActionListener()
        	{
        		public void actionPerformed(ActionEvent e)
        		{
        			MainFrame.exit(0);
                }
        	}
        );
    }

	private void setupGUI()
	{
		Toolkit tk;
		tk = Toolkit.getDefaultToolkit();
		setIconImage(new ImageIcon(getClass().getResource("main.png")).getImage());
		setTitle("Login");
		setSize(new Dimension(tk.getScreenSize().width/2,tk.getScreenSize().height/2));
		setLocationRelativeTo(null);
		setResizable(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
    

	protected String login(String[] details) throws SQLException
	{
		String query;
		Statement st;
		ResultSet rs;
		
		query = String.format("SELECT * FROM %s c WHERE c.email = '%s' AND c.password = '%s' COLLATE utf8_bin", details[2], details[0], details[1]);
		st = connection.createStatement();
		rs = st.executeQuery(query);
		
		if(!rs.next())
		{
			return null;
		}
		else
		{
			return rs.getString(2);
		}
	}
}
