package comp4410.booksRUs;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ResultPanel extends JPanel implements MouseListener
{
	public Color lightGray = new Color(200,200,200,255);

	public ResultPanel() 
	{
		addMouseListener(this);
		setBackground(Color.white);
	}	

	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		this.setBackground(lightGray);
	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		this.setBackground(Color.white);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
}
