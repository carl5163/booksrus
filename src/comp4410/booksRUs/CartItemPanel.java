package comp4410.booksRUs;

import java.awt.Color;
import java.awt.Font;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.GroupLayout;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
@SuppressWarnings({ "serial", "unused" })
public class CartItemPanel extends JPanel {
	
	int pid;
	private double price;
	private int quantity;
	private int inStock;
	private boolean dvd;
	private String title;
	private JTextField tfQuantity;
	private CartPanel parent;
	
	public CartItemPanel(boolean dvdPanel, int pid, String title, double price, int quantity, int inStock, CartPanel p) {
		this.title = title;
		this.pid = pid;
		this.price = price;
		this.quantity = quantity;
		this.inStock = inStock;
		dvd = dvdPanel;
		parent = p;
		setupPanel(dvdPanel);
	}
	
	
	public void setupPanel(boolean dvdPanel) {
		
		JLabel lblTitle = new JLabel(String.format("<HTML><U>%s</U></HTML>", title));
				
		JLabel lblPriceLabel = new JLabel("Price:");
		lblPriceLabel.setFont(new Font(lblPriceLabel.getFont().getFontName(), Font.BOLD, lblPriceLabel.getFont().getSize()));
		lblPriceLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel lblPrice = new JLabel(String.format("$%.2f", price));
		lblPrice.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel lblQuantity = new JLabel("Quantity:");
		lblQuantity.setHorizontalAlignment(SwingConstants.RIGHT);
		lblQuantity.setFont(new Font(lblQuantity.getFont().getFontName(), Font.BOLD, lblQuantity.getFont().getSize()));
		
		tfQuantity = new JTextField(Integer.toString(quantity));
		tfQuantity.setHorizontalAlignment(SwingConstants.RIGHT);
		tfQuantity.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				tfQuantity.setBackground(Color.WHITE);
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				tfQuantity.setBackground(Color.WHITE);				
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				tfQuantity.setBackground(Color.WHITE);
			}
			
		});
		
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		layout.setHorizontalGroup(
			layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addGroup(layout.createParallelGroup()
						.addComponent(lblTitle)
						.addGroup(layout.createSequentialGroup()
							.addComponent(lblPriceLabel)
							.addComponent(lblPrice)
							.addComponent(lblQuantity)
							.addComponent(tfQuantity)
						)
					)
				)
		);
		layout.setVerticalGroup(
			layout.createSequentialGroup()
				.addComponent(lblTitle)
				.addGroup(layout.createParallelGroup()
					.addComponent(lblPriceLabel)
					.addComponent(lblPrice)
					.addComponent(lblQuantity)
					.addComponent(tfQuantity)
				)
		);
		setLayout(layout);
		layout.linkSize(SwingConstants.VERTICAL, lblQuantity, tfQuantity, lblPriceLabel, lblPrice);
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int i) {
		quantity++;
	}


	public void updateQuantity() {
		tfQuantity.setText(Integer.toString(quantity));
	}
	
	public boolean setQuantity() {
		try {
			quantity = Integer.parseInt(tfQuantity.getText().trim());
		} catch(NumberFormatException e) {
			tfQuantity.setText(Integer.toString(quantity));
			tfQuantity.setBackground(Color.RED);
			return false;
		}
		return true;
	}


	public boolean validateQuantity(Connection con) {
			
		String query;
		Statement st;
		ResultSet rs;
		
		try 
		{
			query = "Select quantity From Products WHERE productId = " + pid;
			st = con.createStatement();
			rs = st.executeQuery(query);
			while(rs.next()) {
				int invQ = rs.getInt(rs.findColumn("quantity"));
				if(invQ < quantity) {
					tfQuantity.setText(Integer.toString(invQ));
					tfQuantity.setBackground(Color.YELLOW);
					parent.updateTotals();
					return false;
				}
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return true;
	}


	public double getTotal() {
		return quantity*price;
	}


	public double getShip() {
		int shipRate = dvd ? 1 : 2;
		return shipRate*quantity;
	}

	
}
