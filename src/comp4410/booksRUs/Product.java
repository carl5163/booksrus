package comp4410.booksRUs;

public class Product 
{
	int productId;
	double price;
	int quantity;
	String title;
	int year;
	
	public Product(int pid, double p, int q, String t, int y)
	{
		productId = pid;
		price = p;
		quantity = q;
		title = t;
		year = y;
	}
}
